import React from 'react';
import { Redirect,Route } from "react-router-dom";
import AdminWrapper from '../containers/AdminWrapper';

const Admin = ({component:Component,isAuthed,...rest}) => {
return(
<Route  {...rest} render={routeProps=>(
    // isAuthed ? (
<AdminWrapper>
    <Component {...routeProps}/>
</AdminWrapper>
    // ):(

    //     <Redirect to={{pathname:'/login' ,location:{ from:routeProps.location}}} />
    // )

)

}  /> 
       
)
}

export default Admin