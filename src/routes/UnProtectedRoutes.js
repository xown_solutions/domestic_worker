import React from 'react';
import { Route } from "react-router-dom";
import GuestWrapper from '../containers/GuestWrapper';

const Guest = ({component:Component,...rest}) => {
return(
    <Route  {...rest} render={routeProps=>(

    <GuestWrapper>
        <Component {...routeProps}/>
    </GuestWrapper>
       
    )
    
    }  /> 
)
}

export default Guest