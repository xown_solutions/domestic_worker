import React, { useState, useRef } from "react";
import Button from "../../elements/Button/Button";
import SignUp from "../SignUp/SignUp";
import SignIn from "../SignIn/SignIn";

const Modal = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [signInIsOpen, setSignInIsOpen] = useState(false);
  const handleClose = () => setModalIsOpen(false);
  const handleShow = () => setModalIsOpen(true);
  return (
    <>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={handleClose}
        id="SignUp"
        overlayClassName="overlay"
      >
        <Button onClick={handleClose} className="modal-close">
          <span>x</span>
        </Button>
        <SignUp />
      </Modal>
      <Modal
        isOpen={signInIsOpen}
        onRequestClose={() => setSignInIsOpen(false)}
        id="SignIn"
        overlayClassName="overlay"
      >
        <Button onClick={() => setSignInIsOpen(false)}>
          <span>x</span>
        </Button>
        <SignIn />
      </Modal>
    </>
  );
};

export default Modal;
