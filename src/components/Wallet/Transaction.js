import React from 'react';
import Card from "../../elements/Card/Card"
import { Table} from "react-bootstrap";
const Transaction = () => {
  return (
    <Card className="mt-4 col-12">
    <Table  hover >
 <thead>
   <tr className="text-light-orange">
    
     <th>date</th>
     <th>description</th>
     <th>amount</th>
    
   </tr>
 </thead>
 <tbody className=" text-dark-grey" >
   <tr>
 
     <td>1 Jul 2020</td>
     <td>Cleaning appointment with...</td>
     <td className="text-green">N1000 ...</td>
   
   </tr>
   <tr>
 
 <td>1 Jul 2020</td>
 <td>Cleaning appointment with...</td>
 <td className="text-red">N1000  ...</td>
 

</tr>
<tr>
 
 <td>1 Jul 2020</td>
 <td>Cleaning appointment with...</td>
 <td className="text-green">N1000  ...</td>


</tr>
     
<tr>
 
 <td>1 Jul 2020</td>
 <td>Cleaning appointment with...</td>
 <td className="text-red">N1000  ...</td>


</tr>
 </tbody>
</Table>


</Card> 

  )
}



export default Transaction