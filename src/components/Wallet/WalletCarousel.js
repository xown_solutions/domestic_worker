import React from 'react';
import {  Row} from "react-bootstrap";
import Card from "../../elements/Card/Card"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const WalletCarousel= () => {
    var settings = {
     
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
     
        
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 2,
              initialSlide: 3,
              infinite: true,
            
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          
        ]
    }
   
return(


  <Slider {...settings}  >
   
    <div  > 
     <Card account className=" account mr-4 mb-4">
       <h6>Total Balance</h6>
      <p className="d-flex justify-content-around"> ₦ <h1>25,500.</h1>65</p>
     </Card>
    
    
     </div>
     <div  > 
 
 
     <Card account  className=" account mr-4 mb-4">
       <h6>Referral Balance</h6>
       <p className="d-flex justify-content-around"> ₦  <h1>5,000.</h1>65</p>
     </Card>
   
 
     </div>
     <div >
   
     <Card account  className=" account mr-4 mb-4">
     

       <h6>WalletBalance</h6>
       <p className="d-flex justify-content-around"> ₦ <h1>20,500</h1>.00</p>
     </Card>
 
     </div>
   
   
  </Slider>

)
}

export default WalletCarousel