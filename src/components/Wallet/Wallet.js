import React,{useState} from 'react';
import Card from "../../elements/Card/Card"
import Button from "../../elements/Button/Button";
import {  Row,Form,Col, Tabs} from "react-bootstrap";
import { Link } from "react-router-dom";
import Referrals from "./Referrals";
import Transactions from "./Transaction";
import WalletCarousel from "./WalletCarousel";
import "./wallet.css"

const Wallet = () => {
  const [key, setkey] = useState("transactions");
return(
  <>
    <Row className="space "><h6  className=" text-orange heading-01">  Wallet</h6></Row> 
   <WalletCarousel/>
     <Row className="space" >
     
     <Button className=" wallet-button mr-4  mb-4">
                <Link to="/wallet" >
                  {" "}
                  + ADD TO WALLET
                 
                </Link>
              </Button>
              <Button className=" wallet-button  mr-4 mb-4">
                <Link to="/withdraw">
                  {" "}
                  WITHDRAW
                 
                </Link>
              </Button>
     </Row>
   
     
     <Row  className="wallet-tab">
   
     <Tabs
          id="wallet"
          activeKey={key}
          onSelect={(k) => setkey(k)}
          className=" wallet-nav nav-tabs col-md-8 col-lg-12 col-sm-12 d-flex justify-content-start signUp"
        >
          <div eventKey="transactions"  >
            <Row className="space ">

<Form.Group as={Col} sm="8" controlId="transactions" className="mt-2 text-dark-grey">
              
                   <Form.Control
                     as="select"
                     name="transactions"
                     onChange=""
                     required
                     className="grey wallet col-10"
                   
                   >
                      <option value="" >All Transactions</option>
                      <option value="" >This Month</option>
                      </Form.Control>
                  
                      </Form.Group>   
             </Row> 
             </div>
          <div eventKey="transactions" title="History"  >
            <Transactions />
          </div>
          <div eventKey="referrals" title="Referrals"  >
            <Referrals/>
          </div>
        </Tabs>
        <Card  className="referrals ">
 <div className="card-header">
   <h6 className="heading-01 white">Referrals</h6>
   Get amazing bonuses when you refer DW to our platform and they pay for any DW plan
   </div>

</Card>
     </Row>
  </>
)
}

export default Wallet