import React from "react";
import dateformat from "dateformat";
import footerlogo from "../../assets/images/logo/footerlogo.png";
import styled from "styled-components";
//import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";

import { Link } from "react-router-dom";
import { Col, Form, InputGroup } from "react-bootstrap";
import { FaTwitter, FaInstagram } from "react-icons/fa";
import { FiFacebook } from "react-icons/fi";
import { IconContext } from "react-icons";

const FooterStyle = styled.footer`
  color: #fff;
  a {
    color: #ffff;
    &:hover {
      color: #707070 !important;
      text-decoration: none;
    }
  }
  .base {
    background: #F26527;
  }
`;
const today = new Date();
const Footer = () => {
  return (
    <div>
      <FooterStyle>
        <footer >
          <div className="main">
            <div className="d-flex flex-wrap f-margin">
              <Col
                xs={12}
                md={6}
                lg={4}
                className="c p-3 text-xsm-center text-lg-left mb-4"
              >
                <img
                  alt=""
                  src={footerlogo}
                  height="30"
                  className="brand d-inline-block align-top mb-4"
                />
                <p>
                  Domestic workers is the leading online platform for quality
                  house hold staff for your needs (both as an individual,
                  corporate or an agency). Our aim is to provide seamless
                  engagement of premium house hold staff.
                </p>
                <p>
                  <i className=" edit-icon icon-location-pin left"></i> 3rd
                  Floor, 19, Toyin street, Ikeja, Lagos, Nigeria
                </p>
                <p>
                  {" "}
                  <Link to="tel:+234-9088882828">
                    <span>090 888 2828</span>
                  </Link>&nbsp;
                 (MON-FRI &nbsp;09:00am to 05:00pm){" "} <Link to="mailto:help@domesticworker.com">
                    &nbsp;&nbsp;<span>HELP@DOMESTICWORKER.COM.NG</span>
                  </Link>
                </p>
                <p>
                  {" "}
                 
                </p>
              </Col>
              <Col className="d-flex justify-content-center text-xsm-center d-flex flex-wrap mb-4">
                <div>
                  <h6>Subscribe To Our Newsletter</h6>
                  <p>Get Updates On New offers And Exciting Promos </p>

                  <InputGroup>
                    <input
                      className="form-control"
                      type="email"
                      placeholder="Email Address"
                      required
                      id="email"
                      arial-label="Email Address*"
                    />
                    <InputGroup.Append>
                      <Button>SUBSCRIBE</Button>
                    </InputGroup.Append>

                    <Form.Control.Feedback type="invalid">
                      Please provide a valid Address.
                    </Form.Control.Feedback>
                  </InputGroup>
                </div>
              </Col>
              <Col xs={12} md={12}   lg={4} className=" mr-0 mb-4 d-flex flex-wrap ">
                <Col xs={6} sm={6}>
                  <h6>Quick Links</h6>

                  <p>
                    <Link to="/how_it_works">HOW IT WORKS</Link>
                  </p>
                  <p>
                    <Link to="/blog">BLOG</Link>
                  </p>
                  <p>
                    <Link to="link" href="/">
                      SEARCH FOR DW
                    </Link>
                  </p>
                  <p>
                    <Link to="/faq">FAQ</Link>
                  </p>
                  <p>
                    <Link to="/faq">DW ACADEMY</Link>
                  </p>
                </Col>
                <Col xs={6} sm={6}>
                  <h6>Company</h6>
                  <p>
                    <Link to="/contact_us">CONTACT US</Link>
                  </p>
                  <p>
                    <Link to="/about_us">ABOUT</Link>
                  </p>
                  <p>
                    <Link to="/work_with_us">WORK WITH US</Link>
                  </p>
                  <p>
                    <Link to="/terms_and_conditions">TERMS AND CONDITIONS</Link>
                  </p>
                  <p>
                    <Link to="press_enquiries">PRESS ENQUIRIES</Link>
                  </p>
                  <p>
                    <Link to="/faq">FAQ</Link>
                  </p>
                  
                </Col>
              </Col>
            </div>
          </div>
          <div className="base d-flex flex-wrap  col-12 ">
            <Col className="p-2 text-left ml">
              <p className="small">
                &copy; {dateformat(today, "yyyy")}
                <Link to="/">&nbsp;Domestic Workers</Link>
              </p>
            </Col>
            <Col className="p-2 text-right mr">
              <Link
                to="https://instagram.com/xown_solution/"
                target="_blank"
                aria-label="Instagram"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons" }}>
                  <FaInstagram />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://twitter.com/xown_solution/"
                target="_blank"
                aria-label="twitter"
                className="pr-3"
              >
                 <IconContext.Provider value={{ className: "icons" }}>
                  <FaTwitter />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://facebook.com/XownSolutionsLimited/"
                target="_blank"
                aria-label="Facebook"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons" }}>
                  <FiFacebook />
                </IconContext.Provider>
              </Link>
            </Col>
          </div>
        </footer>
      </FooterStyle>
    </div>
  );
};

export default Footer;
