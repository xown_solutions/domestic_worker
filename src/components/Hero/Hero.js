import React from "react";
import { Link } from "react-router-dom";
import { Row} from "react-bootstrap";
import Button from "../../elements/Button/Button";
import banner from "../../assets/images/homebanner.jpg";

const Hero = () => {
  return (
    <>
    <Row className="hero d-flex justify-content-center">
      <img src={banner} alt="banner" className="banner" />
     
     
      <div className=" hero-inner ">
        
        <Row className=" bold  mt-2">
          <h6>Engage the best</h6>
        </Row>
        <Row className="d-flex flex-wrap ">
          <h6 className="grey-color">Household Staff</h6> &nbsp;&nbsp;
          <h6>for the job</h6>
        </Row>

        <Row className="d-flex flex-wrap ">
          <Button className="mr-2">
            <Link to="/signUp" className="small">
              START HIRING
            </Link>
          </Button>
          <Button grey>
            <Link to="/signUp" className="small">
              START WORKING
            </Link>
          </Button>
        </Row>
      </div>
    </Row>
    </>
  );
};
export default Hero;
