import React from "react";
import {NavLink} from 'react-router-dom';
//import SignIn from "../components/SignIn/SignIn.js";
import { FaBars ,FaEnvelope,FaCalendarAlt,FaMedal,FaWallet, FaUser, FaRocket} from "react-icons/fa";
import { GiHeartBeats} from "react-icons/gi";
import { MdDashboard ,  MdSettings} from "react-icons/md";
import { IconContext } from "react-icons";

const AdminSidebar= ({toggler}) => {

return (
 
<div id="sidebar-wrapper" >
<ul className="sidebar-nav" style={{marginLeft:"0"}}>
   <li className="sidebar-brand">
       
           <p onClick={toggler}  id="menu-toggle"  style={{marginLeft:"20px", float:"right"}} > 
           <i className="fa fa-bars " style={{fontSize:"20px !Important"}} aria-hidden="true"></i>
           <IconContext.Provider value={{ className: "icons" }}>
                 <FaBars />
               </IconContext.Provider>
             
              </p>
       
   </li>
   <li><p style={{fontSize:'10px'}}>Hi first name</p></li>
   <li>
  
      <NavLink to='/admin/dashboard' activeClassName='active'> 
      <IconContext.Provider value={{ className: "" }}>
                 <MdDashboard />
               </IconContext.Provider>  
                <span className="sider" style={{marginLeft:"10px"}}>Dasboard</span>     
        </NavLink> 
   </li>
   <li>
      <NavLink to='/admin/message_board' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
                 <FaEnvelope/>
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> Messages</span>
       </NavLink>  
   </li>
   <li>
      <NavLink to='/admin/my_calendar' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
                 <FaCalendarAlt/>
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> My Calender</span>
       </NavLink>  
   </li>
   
   <li>
      <NavLink to='/admin/trainings' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <FaMedal />
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> Trainings</span>
       </NavLink>  
   </li> 
   <li>
      <NavLink to='/admin/billings' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <FaWallet />
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> Billings</span>
       </NavLink>  
   </li>
   <li>
   <NavLink to='/admin/medicals' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <GiHeartBeats />
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> Medicals</span>
       </NavLink>  
   </li>
   <li>
      <NavLink to='/admin/on_demand' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <FaRocket/>
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> On Demand</span>
       </NavLink>  
         </li>
         <li>
      <NavLink to='/admin/users_record' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <FaUser/>
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}>User Data</span>
       </NavLink>  
         </li>
           <li>
      <NavLink to='/admin/settings' activeClassName='active'> 
               <IconContext.Provider value={{ className: "" }}>
               <MdSettings/>
               </IconContext.Provider>
                <span className="sider" style={{marginLeft:"10px"}}> Settings</span>
       </NavLink>  
   </li>
</ul>
</div>
 
)
}
export default AdminSidebar;