import React, { Component } from 'react'
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";
import Card from "../../elements/Card/Card";
 import { 
 Form,
  Select,
  Space,
 Col


} from 'antd';
const { Option } = Select;


class Filter extends Component {
  constructor(){
      super();
    this.state = {
       
    };
 
  }

    


   

  render() {
    return (
        <Card>
         
       
      <Form
      name="filter"
  className="flex row filter"
  gutter={[16, 16]}
    >
      
   <Col  className="col-sm-6 col-md-3">
       <h6   >Domestic Worker   </h6> 
        <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="Nanny">Nanny</Option>
          
        </Select>
        </Col>
     <Col  className="col-sm-6 col-md-3">
       <h6   > Verifications  </h6> 
        <Select placeholder="All"  mode="multiple"  className="w-100  gutter-row" >
          <Option value="ID verified">ID verified</Option>
          <Option value="MEDICALS verified">MEDICALS verified</Option>
        </Select>
        </Col>
        <Col  className="col-sm-6 col-md-3" >
        <h6 > Location  </h6> 
        <Select placeholder="All States"  className="w-100  gutter-row" >
          <Option value="Lagos">Lagos</Option>
          
        </Select>
        <Select placeholder="City"  className="w-100  gutter-row" >
          <Option value="Ikeja">Ikeja</Option>
          
        </Select>
        </Col>
     <Col  className="col-sm-6 col-md-3">
        <h6 >  Categories </h6> 
        <Select placeholder="Categories"  className="w-100  gutter-row" >
          <Option value="All types">All types</Option>
          
        </Select>
        </Col>
     
    </Form>
      
       </Card>
 
    )
  }
}

export default Filter