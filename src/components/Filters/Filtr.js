import React ,{useState} from 'react'
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";
import Card from "../../elements/Card/Card";
import {  Col,Form } from "react-bootstrap";
import { Multiselect } from 'multiselect-react-dropdown';

const Filter = () => {
  const [list, setList] =useState([{ value:' ID VERIFIED', selected:true }, { value: ' GAURANTOR ' }, { value:' MEDICALS' },
  { value:' ADDRESS VERIFIED' }, { value:' REFREE ' }]) 
  // const [selectedValue, setSelectedValue] =useState([{ value:'Two', selected:true }]) 
  const onSelect=(selectedList, selectedItem)=> {
    console.log(selectedItem)  
}
 
const onRemove=(selectedList, removedItem)=>{
  console.log(removedItem)  
}
    return (
        <Card>
         <div className="d-flex flex-wrap ">
              <Col
               xs={12} md={3}
                className=" text-left"
              >
                  <p>Domestic Workers</p>
                 <Form.Group as={Col} sm="12" controlId="dw">
                     
                        <Form.Control
                          as="select"
                          name="dw"
                           //onChange={this.handleChange}
                          required
                          className='result'>
                          <option value="" >All</option>
                        {/*   {this.props.categories &&
                this.props.categories.categories &&
                this.props.categories.categories.map((category) => (
                          <option value={category.id} key={category.id}>
                            {category.name}
                          </option>
                ))} */}
                        </Form.Control>
               
                    
                    </Form.Group>
              </Col>
              <Col xs={12} md={3} className=" mr-0  d-flex flex-wrap">
                <p>Verifications</p>
                <Multiselect 
                options={list} // Options to display in the dropdown
        //selectedValues={selectedValue} // Preselected value to persist in dropdown
        onSelect={onSelect} // Function will trigger on select event
        onRemove={onRemove} // Function will trigger on remove event
        displayValue="Verifications" // Property name to display in the dropdown options
        showCheckbox={true}
        />
                <div>
                <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
            GAURANTOR 
            </button>
            <button disabled className="small mb-2">
            MEDICALS
            </button>
            <button disabled className="small mb-2">
            ADDRESS VERIFIED
            </button>
                </div>
               
              </Col>
              <Col xs={12} md={3} className=" mr-0  d-flex flex-wrap ">
              <p>Location</p>
              <Form.Group as={Col} sm="12" controlId="states">
                     
                        <Form.Control
                          as="select"
                          name="states"
                          //onChange={this.handleChange}
                          required
                          className='result'>
                          <option value="" >All States</option>
                        {/*   {this.props.categories &&
                this.props.categories.categories &&
                this.props.categories.categories.map((category) => (
                          <option value={category.id} key={category.id}>
                            {category.name}
                          </option>
                ))} */}
                        </Form.Control>
                     
                     
                    </Form.Group>
                    <Form.Group as={Col} sm="12" controlId="city">
                     
                        <Form.Control
                          as="select"
                          name="city"
                          //onChange={this.handleChange}
                          required
                          className='result' >
                          <option value="" >City</option>
                        {/*   {this.props.categories &&
                this.props.categories.categories &&
                this.props.categories.categories.map((category) => (
                          <option value={category.id} key={category.id}>
                            {category.name}
                          </option>
                ))} */}
                        </Form.Control>
                     
                     
                    </Form.Group>
              </Col>
              <Col xs={12} md={3} className=" mr-0  d-flex flex-wrap ">
              
                    <Form.Group as={Col} sm="12" controlId="categories">
                        <p>Categories</p>
                     
                        <Form.Control
                          as="select"
                          name="city"
                          //onChange={this.handleChange}
                          required
                          className='result'>
                          <option value="" >All Types</option>
                        {/*   {this.props.categories &&
                this.props.categories.categories &&
                this.props.categories.categories.map((category) => (
                          <option value={category.id} key={category.id}>
                            {category.name}
                          </option>
                ))} */}
                        </Form.Control>
                     
                     
                    </Form.Group>
              </Col>
            </div>
           
        </Card>
    )
}

export default Filter
