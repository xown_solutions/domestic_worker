import React,{useState} from "react";
import { connect } from "react-redux";
import {useHistory} from 'react-router';
import { FaSearch } from "react-icons/fa";
import { IconContext } from "react-icons";
import { Form, InputGroup, Button } from "react-bootstrap";

const Searchbar = ({handleSearch,onSearchChange,placeholder}) => {
  const history=useHistory();
 /* let [placeholder,setPlaceholder] =useState('Search For domestic workers')
 if(window.location.pathname === '/faq'){
   setPlaceholder('Search a particular question here...')
 } */
  return (
  
      <Form onSubmit={handleSearch} className="d-flex justify-content-center searchBar mt-4">
      <InputGroup className="mb-3  mt-2" >
      {window.location.pathname === '/results'?(null):(<InputGroup.Prepend>
          <Form.Control as="select" className="w-10" onChange={onSearchChange}>
            <option value="">All</option>
            <option value="Nanny">Nanny</option>
            <option value="cleaner">Cleaner</option>
            <option value="Driver">Driver</option>
          </Form.Control>
        </InputGroup.Prepend>)} 
        <Form.Control
          placeholder={placeholder}
          aria-label="Search For domestic workers"
          aria-describedby="basic-addon"
          name="searchField"
          onChange={onSearchChange}
         
        />
        <InputGroup.Append>
        
        {window.location.pathname  ===  '/faq'?(<IconContext.Provider value={{ className: "icons" }}>
              <FaSearch />
            </IconContext.Provider>
        ):(  <Button variant="outline-secondary" type='submit'>
        {" "}
        <IconContext.Provider value={{ className: "icons" }}>
          <FaSearch />
        </IconContext.Provider>
      </Button>
      )}
        
        </InputGroup.Append>
      </InputGroup>
      </Form>
  
  );
};


export default Searchbar;
