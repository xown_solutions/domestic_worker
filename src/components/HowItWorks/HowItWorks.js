import React, { useState, useRef } from "react";
import { Container, Row, Tabs, Col } from "react-bootstrap";
import DwHowItWorks from "./DwHowItWorks";
import EngagerHowItWorks from "./EngagerHowItWorks";
import Button from "../../elements/Button/Button";
import { Link } from "react-router-dom";


const HowItWorks = () => {
  const [key, setkey] = useState("domesticWorker");
  const { how_it_works } = useRef(); 

  return (
    
  
    <Container id={"how_it_works"}  ref={how_it_works} >
      <Row className="d-flex justify-content-center">
        <Col xs={12} md={12} className="mr-auto mt-2">
          <h6 style={{color:"#F26527",fontWeight:"600"}}>HOW IT WORKS</h6>
        </Col>

        <Tabs
          id="howItWorks"
          transition={false} 
          activeKey={key}
          onSelect={(k) => setkey(k)}
          className="nav-tabs"
        >
          <div eventKey="domesticWorker" title="How Do I Engage ?">
          <EngagerHowItWorks />
          <Row
              xs
              xl={12}
              className="mb-4 d-flex flex-wrap justify-content-center"
            >
              <Button className="welcome-button">
                <Link to="/">
                  {" "}
                  START HIRING
                 
                </Link>
              </Button>
            </Row>
          </div>
          <div eventKey="engager" title="How Do I Get Engaged ?">
          <DwHowItWorks />
          <Row
              xs
              xl={12}
              className="mb-4 d-flex flex-wrap justify-content-center"
            >
              <Button className="welcome-button">
                <Link to="/">
                  {" "}
                  START WORKING
                 
                </Link>
              </Button>
            </Row>
          </div>
        </Tabs>
      </Row>
     
    </Container>
 
  );
};
export default HowItWorks;
