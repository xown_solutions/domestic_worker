import React from "react";
import search from "../../assets/images/search.jpg";
import signup from "../../assets/images/signup.jpg";
import review from "../../assets/images/review.jpg";
import engage from "../../assets/images/engage.jpg";
import { Container, Row, Col } from "react-bootstrap";
import Card from "../../elements/Card/Card"

const EngagerHowItWorks = () => {
  return (
    <Container className="howItWorks">
      <Row className="d-flex  justify-content-center text-center mt-2">
      
        <Col xs={12} md={3}  >
            <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={search} alt="search" />
          <h6>Search</h6>
          <p>Search our wide range of DW and services. You can also perform custom searches.</p>
          </Card>
        </Col>
        <Col xs={12} md={3}  >
            <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={signup} alt="signup" />
          <h6>Sign Up</h6>
          <p>Sign up to gain access to DW profiles, reviews and work history.</p>
          </Card>
        </Col>
        <Col xs={12} md={3}  >
            <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={review} alt="review" />
          <h6>Review</h6>
          <p>Check out information on DW to see if He/ She is a right fit for what you need.</p>
          </Card>
        </Col>
        <Col xs={12} md={3}  >
            <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={engage} alt="engage" />
          <h6>Engage</h6>
          <p>Engage various DW workers. Get on-demand services for your home and organization.</p>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
export default EngagerHowItWorks;
