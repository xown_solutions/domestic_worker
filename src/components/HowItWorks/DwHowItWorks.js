import React from "react";

import { Container, Row, Col } from "react-bootstrap";
import Card from "../../elements/Card/Card"

import signup from "../../assets/images/signup.jpg";
import verify from "../../assets/images/verify.jpg";
import engage from "../../assets/images/engageDW.jpg";

const DwHowItWorks = () => {
  return (
    <Container className="howItWorks ">
      <Row className="d-flex  justify-content-center text-center mt-2">
      
        <Col xs={12} md={4} >
        <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={signup} alt="signup" />
          <h6>Sign Up</h6>
          <p>Sign up for free on our platform and start your amazing journey!</p>
          </Card>
        </Col>

        <Col xs={12} md={4} >
        <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={verify} alt="verify" />
          <h6>Get Verified</h6>
          <p>Get verified and increase your chances of better clients and better income</p>
          </Card>
        </Col>
        <Col xs={12} md={4} >
        <Card howItWorks className="col-md-3 mr-4 mb-4">
          <img src={engage} alt="engage" />
          <h6>Meet Clients</h6>
          <p>Engage with clients and start working!</p>
          </Card>
        </Col>
      
      </Row>
    </Container>
  );
};
export default DwHowItWorks;
