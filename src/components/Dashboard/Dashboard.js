import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Container ,Form,ProgressBar} from "react-bootstrap";
import { BiCaretRightCircle} from "react-icons/bi";
import { IconContext } from "react-icons";
import Button from "../../elements/Button/Button";
import Card from "../../elements/Card/Card";
import andriod from '../../assets/images/andriod.jpg'
import ios from '../../assets/images/ios.jpg';
import "./Dashboard.css";

const Dasboard = () => {
  return (
    <Container fluid className="d-flex justify-content-center small mb-4 dashboard">
      <div className="d-flex  flex-wrap m-2">
        

        <Col xs={12} md={7} className="upcoming pr-4" >
        <h5 style={{color:'#F26527'}}>UPCOMING JOBS</h5>
      
    <Form.Control placeholder="Oct 12, 2020 - House cleaning request by Moses " disabled />
    <Form.Control placeholder="Oct 24, 2020 - House cleaning request by Ayodeji Olaoluwa" disabled />
    <Form.Control placeholder="Nov 1, 2020 - House cleaning request by Moses Munguti" disabled />
        <p className='more'>see all jobs +</p>
        </Col>
        <Col xs={12} md={5} >
        <Col className='d-flex justify-content-end'>  
        <Button notify>
       Notifications &nbsp; 31
          </Button></Col>
      
          <Card style={{display:"flex" ,margin:'7px 0' ,padding:'8px '}} className='status'>
          <div className="left">
          <p>Profile Strength</p>
          <h6 className="profile_strength">80%</h6>
          <p style={{color:'#F26527'}}>Very Strong Profile</p>
            </div>
            <div className="vl">
            <p ></p>
<p>Stats </p>
<p>Profile <b>Completed</b></p>
<p>Verification  <b>Luxury</b></p>
<p>Certification  <b>DW level 5</b></p>
<Button>Increase   Strength</Button>
            </div>  
          </Card>
        </Col>
    
    
        

        <Col xs={12} md={7} className="completed pr-4" style={{paddingTop:'15px '}}>
        <h5>COMPLETED JOBS</h5>
      
    <Form.Control placeholder="Oct 11, 2020 - House cleaning for Ade Ahmed" disabled />
    <Form.Control placeholder="Oct 08, 2020 - House cleaning for Joy Eze" disabled />
    <Form.Control placeholder="Aug 25, 2020 - House cleaning for Oyin Damola" disabled />
    <p className='more'>see more completed jobs +</p> 
        </Col>
        <Col xs={12} md={5} >
        <Col style={{display:"flex",margin:'7px 0'}} >
        <Link  >
      <img src={andriod} alt='get it on google'/> 
          </Link>
          <Link className="download">
          <img src={ios} alt='get it on apple'/> 
       </Link>
        </Col>
      
          <Card className='trainings'>
          
          <p>Trainings</p>
          <p><Link to="/trainings"  >
           <IconContext.Provider value={{ className: "icons" }}>
                < BiCaretRightCircle />Go to next lesson:
                </IconContext.Provider>
                </Link>  Improving your interpersonal skills</p>
          <p>progress level</p>
          <p><Form.Control type="range" custom  disabled className='range'/> 10/15</p>
                  
<p>Training level: Level 5 <b>Go to Training</b> </p>

          </Card>
        </Col>
  
    
        

        <Col xs={12} md={7} >
        <h5>LOGS</h5>
      
    <ul>
        <li>~ Updated profile Oct 09, 2020 (job status updated)</li>
        <li>~ Updated profile Aug 06, 2020 (ID verified)</li>
        <li>~ ~ Updated profile Aug 03, 2020 (Changed username and password)</li>
        
    </ul>
    <p className='more'> See all logs +</p>
      
      
        </Col>
        <Col xs={12} md={5} >
       
        </Col>
    </div>
    </Container>
  );
};
export default Dasboard;
