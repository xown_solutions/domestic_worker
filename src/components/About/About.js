import React from 'react';
import { Container, Col, Row } from "react-bootstrap";
import D from '../../assets/images/D.png';
import W from '../../assets/images/W.png';

const About = () => {
return(

      <div className="d-flex flex-wrap justify-content-center mb-4 ">
      <Row  className="a-row">
        

        <Col xs={12} md={6} className="about text">
        <h1>Enhancing trust between household staffs and their employers </h1> 
        <p style={{color:"#707070"}}>Domestic Workers provides an online platform where you can engage verified quality house hold staff for your needs (both as an individual, corporate or an agency.</p>
        </Col>
        <Col xs={12} md={6} className="about">
        <img
            src={D}
            alt="D"
            
          />
        </Col>
      </Row>
      <div style={{ background: "#efefef"}}>
      <Row className="a-row" >
        

        <Col xs={12} md={6} className="about text">
        <h5 >What Keeps Us Inspired </h5> 
        <h6>Our Story</h6>
        <p style={{color:"#707070"}}>Domestic Workers was founded in 2020 by a team of dedicated experts. We decided to create a platform where household staff can be registered, verified, trained and their services engaged by prospective employers who seek quality services and trust.</p>
       
        <h6>Our Core Values</h6>
        <p style={{color:"#707070"}}>We strongly believe in SELF-HONESTY among our stakeholders which include domestic workers, staffs, and employer (also known as engager) so as to promote TRUST and encourage FAIR DEALINGS while providing QUALITY AND RELIABLE SERVICES.</p>
        </Col>
        <Col xs={12} md={6} className="about">
        <img
            src={W}
            alt="w"
            // style={{marginRight:'59px' }}
          />
        </Col>
      </Row>
      </div>
      <Row className="a-row mt-4" >
      <Col xs={12} md={6} className="about bg" >
        <img
            src={W}
            alt="w"
            style={{float:"left" }}
          />
        </Col>

        <Col xs={12} md={6} className="about text" style={{height:'668px'}}>
        <h6>Our Mission</h6>
        <p style={{color:"#707070"}}>We are a team dedicated to ensuring that all parties especially minors are treated well while domestic activities and other related services are being carried out.</p>
       
        <h6>Our Vision</h6>
        <p style={{color:"#707070"}}>Promoting trust, minimizing discrimination and empowering individuals to provide quality and reliable domestic and related services innovating using technology.</p>
        </Col>
    
       
      </Row>
    </div> 
)
}

export default About