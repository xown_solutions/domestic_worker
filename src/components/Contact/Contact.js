import React from 'react';
import { Col, Row,Form  } from "react-bootstrap";
import D from '../../assets/images/D.png';
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";
import { FaEnvelope, FaPhoneAlt, FaMapMarkerAlt} from "react-icons/fa";
import { IconContext } from "react-icons";


const Contact = () => {
return(

      <div className="d-flex flex-wrap justify-content-center mb-4 contact-wrapper" >
      <Row  className="a-row">
        

        <Col xs={12} md={6}  className="about">
          <div style={{maxWidth: '592px'}}>
        <h5>We'd love to hear from you! </h5> 
        </div>
        <Form
              /* noValidate
              onSubmit={this.handleSubmit}
           
              validated={this.state.validated} */
              style={{margin:'50px 0'}} >
          
              {/* <p style={{color:"red"}}>{this.state.msg}</p> */}
              <Form.Row>
                <Form.Group as={Col} col="6" controlId="first_name">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="First name *"
                    //   name="first_name"
                    //   onChange={this.handleChange}
                      min="4"
                      required
                    />
                  </Input>
                  {/* {error.first_name.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.first_name}!!
                    </span>
                  )} */}
                </Form.Group>
                <Form.Group as={Col} col="6" controlId="mobile">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Mobile no *"
                      name="mobile"
                    //   onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {/* {error.mobile.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.mobile}!!
                    </span>
                  )} */}
                </Form.Group>
                </Form.Row>
                <Form.Row>
                <Form.Group as={Col} col="12">
                  <Input>
                    <Form.Control
                      type="email"
                      placeholder="Email *"
                      pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-z]{2,4})$"
                      name="email"
                    //   onChange={this.handleChange}
                      required
                    />
                  </Input>
                 {/*  {error.email.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.email}!!
                    </span>
                  )} */}
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} col="12" controlId="enquiry">
                  <Input>
                 
                     
                    <Form.Control
                      as="select"
                      name="enquiry"
                    //   onChange={this.handleChange}
                      required
                      > <option value="">Your inquiry is about?</option>
                      {/*    {this.props.identification &&
                      this.props.identification.identification &&
                      this.props.identification.identification.map((identification) => (
                      <option value={identification.id} key={identification.id} > 
                      {identification.name}
                          </option>
                      ) )} */}
                    </Form.Control>
                  
                  </Input>
                  
                 {/*  {error.identity_type_id.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.identity_type_id}!!
                    </span>
                  )} */}
                </Form.Group>
                </Form.Row>

                <Input>
                <Form.Group controlId="message">
               
                <Form.Control as="textarea" rows="6"   placeholder="Message"/>
                </Form.Group>
                        
              <Form.Row className="d-flex justify-content-left">
                <Button type="submit"name="submit" >SEND MESSAGE</Button>
              </Form.Row>
            </Input>
            </Form>
      
        </Col>
        <Col xs={12} md={6} className="contact">
        <h6>
        
             Reach out to us directly</h6>
        <p>
            <IconContext.Provider value={{ className: "contact_icons" }}>
                  <FaMapMarkerAlt />
                </IconContext.Provider>3rd Floor, 19, Toyin Street, Ikeja, Lagos.</p> <p><IconContext.Provider value={{ className: "contact_icons" }}>
                  <FaPhoneAlt />
                </IconContext.Provider> +234 908 888 2828</p>
        <p>
        <IconContext.Provider value={{ className: "contact_icons" }}>
                  <FaEnvelope />
                </IconContext.Provider>
            help@domesticworkers.com.ng
            </p>
        <img
            src={D}
            alt="D"
           style={{width:'100%',marginTop:'2em'}} 
          />
        </Col>
      </Row>
    
      
    </div> 
)
}

export default Contact