import React, {useRef ,useState,useEffect } from "react";
import {useHistory} from 'react-router';
import { connect } from "react-redux";

import { login } from "../../redux/actions/loginAction";
import { Navbar } from "react-bootstrap";
import logo from "../../assets/images/logo/logo.png";
import styled from "styled-components";
 import SignedInLinks from "./SignedInLinks"; 
import SignedOutLinks from "./SignedOutLinks";
import { displayDW } from "../../redux/actions/displayDwAction";
import {SearchSucess } from "../../redux/actions/searchAction";


const NavStyle = styled.nav`

  .navbar-nav .nav-link {
    color: #F26527;
  }
    &:hover {
      color: #707070;
    }
  }
`;
const Header = ({login, dwData, search,fetchDwData,fetchFilteredData}) => {
  useEffect(() => {
    fetchDwData();
  }, [fetchDwData]);
  const [searchField ,setSearchField] = useState("");
  const [filter,setFilter] = useState([]);
  const history=useHistory();

  const onSearchChange = (event)=>{
    setSearchField(event.target.value) ;
  console.log(searchField)
 
 }

 const handleSearch =(event)=>{
event.preventDefault();

 const filteredDw =dwData.dws.filter((dw) => {
    let name=dw.first_name + dw.last_name +dw.dw_type+dw.location
    return name.toLowerCase().includes(searchField.toLowerCase())
  })
  
  setFilter(filteredDw)
 
  fetchFilteredData(filteredDw);
  history.push('/results')
 /*  if(search.searchResult.length !== 0 ||search.searchResult.length > 0){
  history.push('/results');         
}*/
  }
  return (
    <NavStyle>
      <Navbar expand="md" fixed="top" className=" form" style={{height:'64px'}}>
        <Navbar.Brand href="/">
          <img
            alt="logo"
            src={logo}
            
            height="30"
            className="d-inline-block align-top"
          />
          
        </Navbar.Brand>
        <div className="d-flex w-70 searchBar mt-4" ></div>
{login.isAuthorized ?(<SignedInLinks onSearchChange={onSearchChange} handleSearch={handleSearch}/>):(<SignedOutLinks onSearchChange={onSearchChange} handleSearch={handleSearch}/>)}
      
      </Navbar>
    </NavStyle>
  );
};

const mapStateToprops = (state) => {
  return { 
    login:state.login,
    dwData: state.displayDw,
    search: state.search,
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    auth: (users_credential) => dispatch(login(users_credential)),
    fetchDwData: () => dispatch(displayDW()),
  
    fetchFilteredData: (search_result) => dispatch(SearchSucess(search_result))
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(Header);

