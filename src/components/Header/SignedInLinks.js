import React, { useRef }  from "react";
import {useHistory} from 'react-router';
import { NavLink,Link } from "react-router-dom";
import member from "../../assets/images/member/member3.jpg";
import Button from "../../elements/Button/Button";
import { Nav, Navbar ,Row} from "react-bootstrap";
import Card from '../../elements/Card/Card';
import SearchBar from '../SearchBar/Searchbar';

const SignedInLinks = ({onSearchChange,handleSearch}) => {
  const signOut = useRef();
  const menu= useRef();
  const history=useHistory();
  return (
    <>
    <Navbar.Toggle aria-controls={menu} />
    <Navbar.Collapse  ref={menu}>
     
        
          
    {window.location.pathname && history.location.pathname  === '/results' ? (<SearchBar placeholder='Find Domestic Workers' onSearchChange={onSearchChange} handleSearch={handleSearch} />):(null)}
     <Nav className="ml-auto m-2 ">
        
      
          <Button
            onClick={() => (signOut.current.style.display = "flex")}
            grey
            className="small mr-2"
          >
          Log Out
          </Button>
        
        <Link to='/dashboard' className='avatar'>
          <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
              </Link>
      </Nav>
    </Navbar.Collapse>
    <div className="modal " id="SignOut" ref={signOut}> 
       
       <div className="modal-content " style={{height:'20%'}}>
       <span   onClick={() => (signOut.current.style.display = "none")} className="modal-close"> &times;</span>
        
         <Row className="d-flex flex-wrap justify-content-center" >
         <Link to="/signin" >
           <p>Are Sure You Want To Logout ?</p>
          <Button className="mr-2"  style={{width:'100px'}}>
           
              YES
        
          </Button>
          </Link>
          <Button grey onClick={() => (signOut.current.style.display = "none")} style={{width:'100px'}}>
          
              NO
           
          </Button>
        </Row>
       
       </div>
     </div>
    </>
  );
};

export default SignedInLinks;
