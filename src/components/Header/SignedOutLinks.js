import React, { useRef ,useState,useEffect } from "react";
import {useHistory} from 'react-router';
import {Link} from 'react-router-dom';
import { connect } from "react-redux";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import Button from "../../elements/Button/Button";

import SearchBar from '../SearchBar/Searchbar';
import { displayDW } from "../../redux/actions/displayDwAction";

import {SearchSucess } from "../../redux/actions/searchAction";

const Signedoutlinks = ({onSearchChange,handleSearch}) => {
/*   const signIn = useRef();
  const signUp = useRef(); */
  const menu= useRef();
  const history=useHistory();
 

  /* const modal = {
    overlay: {
      position: "fixed",
      top: "30px",
      right: 0,
      bottom: 0,
      backgroundColor: "#fff",
      zIndex: 1,
      boxShadow: "10px 10px 10px 0px rgba(0, 0, 0, 0.16)",
      borderRadius: "5px",
      backgroudColor: " #000",
    },
    content: {
      position: "absolute",
      top: "40px",
      left: 0,
      right: 0,
      bottom: "40px",
      background: " rgb(255, 255, 255)",
      overflow: "auto",
      borderRadius: "4px",
      outline: "none",
      padding: "2px",
      
  mix-blend-mode: overlay;
}
    },
  }; */
  
  return (
    <>
      <Navbar.Toggle aria-controls={menu} />
      <Navbar.Collapse  ref={menu}>
       
          
            
       {window.location.pathname && history.location.pathname  === '/results' ? (<SearchBar placeholder='Find Domestic Workers' onSearchChange={onSearchChange} handleSearch={handleSearch} />):(null)}
       
       <Nav className= {`ml-auto m-2 ${history.location.pathname  === '/results'? "searchNav" : ""}`}>
          <Nav.Link href="/#how_it_works">How It Works</Nav.Link>
         
          <Nav.Link href="#blog">Blog</Nav.Link>
          <Nav.Link href="/signUp">I Want To Hire</Nav.Link>
         {/*  <NavDropdown title=" I Want To Hire" id="collapsible-nav-dropdown">
            <NavDropdown.Item href="#hire:industrial">
              Co-operate
            </NavDropdown.Item>
            <NavDropdown.Item href="#hire:individual">
              Individual
            </NavDropdown.Item>
          </NavDropdown> */}
          <Nav.Link href="/signUp">I Want To Work</Nav.Link>
          <Nav.Link href="#ondemand">On-Demand</Nav.Link>
          <div className="col-xsm-6 mb-2">
            <Button
              // onClick={() => (signIn.current.style.display = "flex")}
              grey
              className="small"
            >
            <Link to="/SignIn" className="white">Sign In</Link>  
            </Button>
          </div>
          <div className="col-xsm-6">
            <Button
              // onClick={() => (signUp.current.style.display = "flex")}
              className="small"
            >
            <Link to="/SignUp" className="white">Sign Up</Link>  
            </Button>
          </div>
        </Nav>
      </Navbar.Collapse>
    {/*   <div className="modal " id="SignIn" ref={signIn}> 
       
        <div className="modal-content ">
        <span   onClick={() => (signIn.current.style.display = "none")} className="modal-close"> &times;</span>
          <SignIn />
        </div>
      </div>

      <div className="modal mr-2" id="Signup" ref={signUp}>
     
        <div className="modal-content ">
     
          <span onClick={() => (signUp.current.style.display = "none")} className="modal-close"> &times;</span>
      
          <SignUp />
        </div>
      </div> */}
    </>
  );
};
export default Signedoutlinks
