import React from 'react';

import Testimonial from "../Testimonial/Testimonial"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const CarouselContainer = () => {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
     
        
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
          
      };
   
return(
 <div className="carousel mb-4 mt-4">
<h6 className="testify text-left p-4">Get Inspired With our Engagers Testimonies</h6>
  <Slider {...settings}  >
   
    <div  className="slide"><Testimonial /></div>
    
    <div className="slide"><Testimonial /></div>
    <div className="slide"><Testimonial /></div>
  
   
   
  </Slider>
</div>
)
}

export default CarouselContainer