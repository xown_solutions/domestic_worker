import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import banner1 from "../../assets/images/banner/1.jpg";
import banner2 from "../../assets/images/banner/2.jpg"
import banner3 from "../../assets/images/banner/3.jpg"

const Carousel= () => {
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        cssEase: "linear"/* ,
        appendDots: dots => (
            <div
              style={{
                backgroundColor: "#ffff",
       
                padding: "10px"
              }}
            >
              <ul style={{ margin: "0px" }} > {dots} </ul>
            </div>
          ) */
      };
   
return(
 <div className=" mb-4 mt-4">
<h6 className="testify text-left p-4 text-orange " style={{fontWeight:600}}>Engage Quality Domestic Worker On The Go</h6>
  <Slider {...settings}  >
   <div>  <img src={banner1} alt="banner1" className="slides" /></div>
   <div>  <img src={banner2} alt="banner2" className="slides" /></div>
   <div>  <img src={banner3} alt="banner3" className="slides" /></div>

   
   
  </Slider>
</div>
)
}

export default Carousel