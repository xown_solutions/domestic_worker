import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Container } from "react-bootstrap";
import Button from "../../elements/Button/Button";
import celebrated from "../../assets/images/member/member4.jpg";

const Celebrated = () => {
  return (
    <Container className="d-flex justify-content-center mb-4 celebrated">
      <Row className="m-2">
        <h6 className="text-center col-12 mb-4">Domestic Worker Of The Month </h6>

        <Col xs={12} md={4} className="featured_dw">
          <img
            src={celebrated}
            alt="celebrated"
            
          />
        </Col>
        <Col xs={12} md={8} className="featured_dw">
          <div className="featured">
          <h5>Meet Olayinka</h5>
          <p>Olayinka Omotayo Adebimpe hails from Ibadan, Oyo State. He is married with 3 kids. Olayinka joined DW on the 3rd of July, 2020. Olayinke has been showing great dedication to his work as a househelp and also great integrity. He has gotten positive reviews consequently and keeps on improving himself. Indeed we are proud of you Olayinka. Keep up the good work!</p>
          <Button className="welcome-button">
            <Link to="/profile:id">View profile</Link>
          </Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
};
export default Celebrated;
