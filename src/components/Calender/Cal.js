import React from 'react'
import { DatePicker,theme } from 'react-trip-date';
import {ThemeProvider} from 'styled-components';
import styled from "styled-components";

const Cal = () => {
	
	const  handleResponsive  =  setNumberOfMonth  =>  {
		let  width  =  document.querySelector('.tp-calendar').clientWidth;
		if  (width  >  1600)  {
			setNumberOfMonth(2);
		}  else  if  (width  <  1600  &&  width  >  1200)  {
			setNumberOfMonth(2);
		}  else  if  (width  <  1200)  {
			setNumberOfMonth(1);
		}
	};
	
	
	const  Day = ({  day  }) => {
		return  (
			<>
				<p  className="date">{day.format('DD')}</p>
			
			</>
			);
		};
	
		const Title=({source })=>{
			let titleDay = ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat']
			return (
				<TitleDaysOfWeekStyle>
				{titleDay.map(item=>(
					
				
				<p key={Math.random()}>
				
					{item}
				
					</p>
					
					))}
					</TitleDaysOfWeekStyle>
				)
		}
	
	 const TitleDaysOfWeekStyle = styled.div`
		display: flex;
		text-align: center;
		justify-content: center;
		flex-direction: row-reverse;
		border-bottom: 1px solid #e2e2e2;
		margin: 30px 0 15px 0;
		font-size: 12px;
		flex-wrap:wrap;
	  
		p {
		  width: 40px;
		  margin-bottom: 15px;
		}
	  `;
	 const onChange = date => console.log(date)
  return (



	<ThemeProvider theme={theme} style={{backgroundColor:'#F26527 '}}>
			  
	<DatePicker
	  handleChange={onChange}
	  responsive={handleResponsive}
	 
	  numberOfMonths={1}
	  
	  dayComponent={Day} //custom day component 
	  titleComponent={Title} // custom title of days
	/>
	
	</ThemeProvider>
  )
}



export default Cal