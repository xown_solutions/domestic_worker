

import React,{Component} from 'react'
import {  Calendar,Input, Form,Alert,Badge  }  from 'antd';
import moment from 'moment';
class Events extends Component{
    constructor(){
        super();
        this.state = {
            value: moment('2017-01-25'),
            selectedValue: moment('2017-01-25'),
            list:[]
          };
    };
 
  
    onPanelChange = value => {
      this.setState({ value });
    };
    
    dateCellRender =() =>{
      const listData = this.state.list;
      return (
        <>
        <Form>
        <Form.Item label="Field A">
          <Input placeholder="input placeholder" />
          </Form.Item>
        </Form>
        <ul className="events">
          {listData.map(item => (
            <li key={item.content}>
              <Badge status={item.type} text={item.content} />
            </li>
          ))}
        </ul>
        </>
      );
    }
    onSelect = value => {
      this.setState({
        value,
        selectedValue: value,
        list:[
          { type: 'warning', content: 'This is warning event.' },
          { type: 'success', content: 'This is usual event.' },
        ]
      });
    this.dateCellRender(value)
    };
    render(){
        const { value, selectedValue } = this.state;
        return (
            <>
            <Alert
              message={`You selected date: ${selectedValue && selectedValue.format('YYYY-MM-DD')}`}
            />
            <Calendar value={value} onSelect={this.onSelect} onPanelChange={this.onPanelChange} />
          </>
            )
          }
          
    }
 


export default Events