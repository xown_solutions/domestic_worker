import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {  Row} from "react-bootstrap";

import Button from "../../elements/Button/Button";
import Card from "../../elements/Card/Card";
import Cal from "./Cal";
import "./Calender.css"

	class Calender extends Component {

 

  render() {
    return (
		<>
    <Cal/>
	  <Row>
     
     <Button className="acc-btn mr-4 mb-4  ">
                <Link to="/account">
                  {" "}
				  NEW APPOINTMENT
                 
                </Link>
              </Button>
              <Button className="acc-btn   mb-4 " grey>
                <Link to="/">
                  {" "}
				  SEE ALL APPOINTMENTS
                 
                </Link>
              </Button>
     </Row>
	 <Row className="space "><h6 className=" text-orange heading" >Appointments</h6></Row> 
    <Card className="p-4 mb-4" >
    <ul className="acc-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey float-right ">16 Aug, 2020</p>
    </Card> 
    <Card className="p-4 mb-4" >
    <ul className="acc-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey float-right ">16 Aug, 2020</p>
    </Card> 
    <Card className="p-4 mb-4" >
    <ul className="acc-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey float-right ">16 Aug, 2020</p>
    </Card> 
	  </>
    );
  }
}
export default Calender;