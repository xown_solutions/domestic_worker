import React, { Component } from "react";
import { connect } from "react-redux";
import {toast} from "react-toastify";
// import {useHistory} from 'react-router'; 
import "react-toastify/dist/ReactToastify.css";
import { login } from "../../redux/actions/loginAction";
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";
import Carousel from "../Carousel/Slides";
import { Container, Col,Form ,Row} from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "../../assets/images/logo/logo.png"
import { FaTwitter, FaInstagram } from "react-icons/fa";
import { FiFacebook } from "react-icons/fi";
import { IconContext } from "react-icons";
import dateformat from "dateformat";

toast.configure()

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
     
     
      email: "",
      password: "",
       validated: false,
      error: {
       
        email: "",
       
        password: "",
       
      },
    };
   
  }
  
 
  validateForm = ({ error, ...rest }) => {
    let isValid = true;

    Object.values(error).forEach((val) => {
      val.length > 0 && (isValid = false);
    });
    Object.values(rest).forEach((val) => {
      if (val === null) {
        isValid = false;
      } else {
        isValid = true;
      }
    
    });

    return isValid;
  };
  
  //^[+]*+[(]{0,2}[0-9]{1,5}[)]{0,1}[-\s\./0-9]*$/
  handleChange = (event) => {
    const emailregExp = RegExp(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    );
  
    const { name, value } = event.target;
    let error = { ...this.state.error };
  
    switch (name) {
      
     
      case "email":
        error.email = emailregExp.test(value) ? "" : "Email address is invalid";
        break;
      case "password":
        error.password =
        value.length < 1 ? "Field can not be empty" : "";
        break;
      
      default:
        break;
    }
  
    this.setState(
      {
        [event.target.name]: event.target.value,
        error,
      },
      () => console.log(this.state)
    );
   
  };

  handleSubmit = (event) => {
    event.preventDefault();
   
    const form = event.currentTarget;
   
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();

      toast.error("Can not submit empty data",{postion:toast.POSITION.TOP_CENTER});
      this.state.error.password.length > 0 && toast.error(this.state.error.password,{postion:toast.POSITION.TOP_CENTER});
    } else if (this.validateForm(this.state)) {
      console.info("Valid Form");
      this.setState({ ...this.state, validated: true});
      let input =this.state
      
    } 
  };

  render() {
    const today = new Date();
    const { error } = this.state;
    
    if(this.props.login.token){
     
      toast.success(this.props.login.token.message,{postion:toast.POSITION.TOP_CENTER,autoclose:8000});
      delete  this.props.login.token.message;
  //this.props.history.push("/dashboard"); 
  }  
    else if(this.props.login.error) { 
      
      toast.error(this.props.login.error.message,{postion:toast.POSITION.TOP_CENTER})
      delete  this.props.login.error.message
     /*  Object.keys(this.props.login.error).map((errorMsg)=>(

        this.props.login.error[errorMsg].map((msg)=>{
        toast.error(msg,{postion:toast.POSITION.TOP_CENTER});
          delete   this.props.login.error[errorMsg];
          
         })
  
      ))  */
       
  } 
   
    return (

        <Container fluid className="reg-actions">
        <Row>
       
        <div className="auth-left col-xl-5 col-sm-12">
          <Col xs md={12} >
       
          <Col xs md={12}className=" d-flex flex-wrap justify-content-center" >
             <Link to="/"><img
            alt="logo"
            src={logo}
            
            height="67"
            className="d-inline-block logo"
          />
          </Link> 
          </Col>
         
       < Col xs md={12} className=" d-flex flex-wrap justify-content-center">
          <Form
                noValidate
                validated={this.state.validated}
                onSubmit={this.handleSubmit}
                className="auth"
              >
             
                <Form.Row>
                  
                <h4
                className="text-center mb-4 col-12"
                style={{ color: "#e77a1a", fontWeight: "500" ,backgroundColor:'#fff'}}
              >
                Welcome Back!
              </h4>
                  <Form.Group as={Col} sm="12">
                    <Input>
                      <Form.Control
                        required
                        type="email"
                        placeholder="Email "
                        onChange={this.handleChange}
                        name="email"
                      />
                    </Input>
                    <Form.Control.Feedback>completed!</Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group as={Col} sm="12">
                    <Input>
                      <Form.Control
                        required
                        type="password"
                        placeholder="Password"
                        onChange={this.handleChange}
                        name="password"
                      />
                    </Input>
                    <Form.Control.Feedback>completed!</Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>
                <div className="d-flex justify-content-center mt-4 mb-4 ">
                <Button type="submit"name="submit" disabled={this.props.login.loading }>{this.props.login.loading ? "...loading" : "Sign In"}</Button>
                </div>
                <p  className="text-center grey-color mb-4">
                  Don't have an account?{" "}
                  <Link
                    to="/signUp"
                    style={{ color: "#e77a1a", fontWeight: "500" }}
                  >
                    Sign Up
                  </Link>
                </p>
              </Form>
              </Col>
              <Col className="p-2 mb-2 text-center " xs xl={12}>
              <Link
                to="https://instagram.com/xown_solution/"
                target="_blank"
                aria-label="Instagram"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FaInstagram />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://twitter.com/xown_solution/"
                target="_blank"
                aria-label="twitter"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FaTwitter />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://facebook.com/XownSolutionsLimited/"
                target="_blank"
                aria-label="Facebook"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FiFacebook />
                </IconContext.Provider>
              </Link>
            </Col>
            <Col className="p-2 mb-2 text-center ">
              <p className="small text-orange">
                &copy; {dateformat(today, "yyyy")}
                <Link to="/"  className=" text-orange">&nbsp;Domestic Workers</Link>
              </p>
            </Col>
          </Col>
          </div>
          <Col xs={12} xl={7} className="hide">
            <Carousel/>
          </Col>
        </Row>
       
         
      </Container>
    
    );
  }
}

const mapStateToprops = (state) => {
  return {
    
    login:state.login
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    auth: (users_credential) => dispatch(login(users_credential)),
   
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(SignIn);
