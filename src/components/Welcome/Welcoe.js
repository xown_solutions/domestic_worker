import React ,{useEffect}from "react";
import {connect} from "react-redux";
import { Container, Col, Row } from "react-bootstrap";
import { fetchCategories,fetchCategoriesCount } from "../../redux/actions/fetchCategoriesAction";
import { Link } from "react-router-dom";
import Button from "../../elements/Button/Button.js";
import Member from "../Member/Member";
import styled from "styled-components";
import { FaArrowRight } from "react-icons/fa";
import { IconContext } from "react-icons";

const WelcomeStyle = styled.div`
  background: #efefef;
  .member {
    max-width: 256px;
    max-height: 200px;
    background: #fff;
  }
  .member img {
    max-width: 66px;
  }
  p {
    margin: 0;
  }
  .bar {
    padding: 0px;
  }
  .bar p {
    max-height: 16px;
  }
`;

const Welcome = ({categories,categoriesCount,fetchCategoriesCount,fetchCategories}) => {
  useEffect(() => {
    fetchCategories();
    fetchCategoriesCount();
  }, [fetchCategories,fetchCategoriesCount]);


  Object.keys(categoriesCount).map((count)=>{
  
    categoriesCount[count].map((num)=>{
       console.log(num)
     })

    }) 

  
  
  return (
     <WelcomeStyle>
      <div className="welcome m-2 home">
        <Container
          className="d-flex flex-wrap justify-content-center w-margin"
          fluid
        >
          <Row>
            {" "}
            <div className="mt-2 col-12">
              <h6 >Engage Quality Domestic Workers </h6>
            </div>
            <div className="d-flex fex-wrap col-12 justify-content-center bar small mt-2">
              <p>
              {categories &&
                categories.categories &&
                categories.categories.map((category) => (
                         
                <b style={{ color: "#F26527" }} key={category.id}> {category.name} </b>
                ))}

           { 
              
              /*  Object.keys(categoriesCount).map((count)=>(
  
                categoriesCount[count].map((num)=>(
                  <Button disabled className=" mb-2 mr-1" primarykey={num}>
                    {num}
                    {console.log(num)}
                    </Button>
                ))
  )) */
  }
                <Link to="/dw">
                  <b style={{ color: "#fff" }}>more...</b>
                </Link>
              </p>
            </div>
            <div className="col-12">
              <hr
                className="col-8 "
                style={{
                  color: "#707070",
                  height: "2px",
                  borderWidth: "medium",
                }}
              />
            </div>
            <div className="col-12">
              <Member />
            </div>
            <Col
              xs
              xl={12}
              className="mb-4 d-flex flex-wrap justify-content-center"
            >
              <Button className="welcome-button">
                <Link to="/dw">
                  {" "}
                  See More
                  <IconContext.Provider value={{ className: "icons" }}>
                    <FaArrowRight />
                  </IconContext.Provider>
                </Link>
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
    </WelcomeStyle>
  );
};


const mapStateToprops = (state) => {
  return {
    categories: state.fetchCategories,
    categoriesCount: state.fetchCategoriesCount
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    
    fetchCategories: () => dispatch(fetchCategories()),
    fetchCategoriesCount: () => dispatch(fetchCategoriesCount()),
  };
};
export default connect(mapStateToprops,mapDispatchToprops)(Welcome);

