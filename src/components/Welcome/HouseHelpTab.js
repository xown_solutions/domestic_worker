import React, { useEffect } from "react";

import Card from "../../elements/Card/Card";
import Button from "../../elements/Button/Button";
import { Link } from "react-router-dom";
import { BsStarFill, BsStarHalf } from "react-icons/bs";
import { IconContext } from "react-icons";
import { Col} from "react-bootstrap";
import member from "../../assets/images/member/member3.jpg";
import { FaArrowRight } from "react-icons/fa";

const HouseHelpTab = ({ dwData }) => {
 
  const Dwmember =dwData.loading ?( <h6>loading..</h6>):dwData.error.length  > 0 ?(<h6>{dwData.error}</h6>): dwData.househelp.length > 0? (<>
  <div className="d-flex flex-wrap justify-content-center">
     
  {dwData &&
    dwData.househelp &&
    dwData.househelp.map((dw,id) => (
      <div className="d-flex flex-wrap mb-2 " key={id}>
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
                {dw.first_name} {dw.last_name}{" "}
              </p>
              <p style={{ color: "#e77a1a" }}>{dw.dw_type}</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
      </div>
    ))}
     
</div>
<Col
          xs
          xl={12}
          className="mb-4 d-flex flex-wrap justify-content-center"
        >
          <Button className="welcome-button">
            <Link to="/dw">
              {" "}
              See More
              <IconContext.Provider value={{ className: "icons" }}>
                <FaArrowRight />
              </IconContext.Provider>
            </Link>
          </Button>
        </Col>
        </>):(
    <div style={{height:"320px"}}>
  <p className="text-center" style={{paddingTop:"150px",paddingBottom:"150px"}}>No Househelp Found </p> 
  </div>
  )
  return(
 <>
 {Dwmember}

 </>
  );
};

export default HouseHelpTab;
