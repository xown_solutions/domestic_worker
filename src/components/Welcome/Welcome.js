import React ,{useState,useEffect}from "react";
import {connect} from "react-redux";
import {NavLink} from 'react-router-dom';
import { displayDrivers } from "../../redux/actions/displayDwAction";
import { displayCleaners } from "../../redux/actions/displayDwAction";
import { displayHouseHelps } from "../../redux/actions/displayDwAction";
 import Member from "../Member/Member"
import styled from "styled-components";

import { Container, Row, Col,Nav ,Tab} from "react-bootstrap";

// import Button from "../../elements/Button/Button";
import { Link } from "react-router-dom";
/*import DriverTab from "./DriverTab";
import HouseHelpTab from "./HouseHelpTab";
import CleanerTab from "./CleanerTab";
*/

const WelcomeStyle = styled.div`
  background: #efefef;
  .member {
    max-width: 256px;
    max-height: 200px;
    background: #fff;
  }
  .member img {
    max-width: 66px;
  }
  p {
    margin: 0;
  }
  .bar {
    padding: 0px;
  }
  .bar p {
    max-height: 16px;
  }
`;
const Welcome = ({dwData,fetchCleanersData, fetchHouseHelpsData,fetchDriversData }) => {
  const [key, setkey] = useState("domesticWorker");
  useEffect(() => {
    fetchCleanersData(); 
    fetchHouseHelpsData();
    fetchDriversData(); 
  }, [fetchCleanersData, fetchHouseHelpsData,fetchDriversData ]);

  return (
     
    <WelcomeStyle>
    <div className="welcome m-2 home d-flex flex-wrap">
      <Container
        className="d-flex flex-wrap justify-content-center w-margin"
        fluid
      >
         <div className="mt-2 col-12">
            <h6 >Engage Quality Domestic Workers </h6>
          </div>
      <Row className="d-flex flex-wrap justify-content-center">
      <Tab.Container id="welcome" defaultActiveKey="Househelp" transition={false}>
  <Row>
    <div className="col-12 d-flex flex-wrap justify-content-center tabRow" >
        <Nav >
        <Nav.Item>
          <Nav.Link eventKey="Househelp" >Househelp
          <button disabled className="count mb-2 mr-1">
          {dwData.househelp.length} 
                  </button>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="Driver"  >Driver
          <button disabled className="count mb-2 mr-1">
          {dwData.driver.length} 
                  </button>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="Cleaner"  >Cleaner
          <button disabled className="count mb-2 mr-1">
          {dwData.cleaner.length} 
                  </button>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <NavLink to="/domestic_workers" >More</NavLink>
        </Nav.Item>
        </Nav>
    </div>
    <Col sm md={12} className="mt-2">
      <Tab.Content     >
        <Tab.Pane eventKey="Househelp">
        <Member dwData={dwData}/>
     {/*    <HouseHelpTab dwData={dwData}/> */}
        </Tab.Pane>
        <Tab.Pane eventKey="Driver">
        <Member dwData={dwData}/>         
      {/*   <DriverTab dwData={dwData}/> */}
        </Tab.Pane>
        <Tab.Pane eventKey="Cleaner">
        <Member dwData={dwData}/>
      {/*   <CleanerTab dwData={dwData} /> */}
       </Tab.Pane>
      </Tab.Content>
    </Col>
  </Row>
</Tab.Container>
       
     {/*    <Tabs
          id="welcm"
          transition={false} 
          onSelect={(k) => setkey(k)}
          className="nav-tabs col-12 justify-content-center"
          
          defaultActiveKey="Househelp"
        >
          <div eventKey="Househelp" title="Househelp" >

          <HouseHelpTab />
         
           
          </div>
          <div eventKey="Driver" title="Driver">
              
          <DriverTab />
        
          </div>
          <div eventKey="Cleaner" title="Cleaner">
          <CleanerTab />
         
          </div>
          <div eventKey="More" title="More">
          </div>
        </Tabs>
         */}
      </Row>
     
    </Container>
    </div>
    </WelcomeStyle>
 
  );
};
const mapStateToprops = (state) => {
    return {
      dwData: state.displayDw,
   
    };
  };
  const mapDispatchToprops = (dispatch) => {
    return {
      fetchDriversData: () => dispatch(displayDrivers()),
      fetchHouseHelpsData: () => dispatch(displayHouseHelps()),
      fetchCleanersData: () => dispatch(displayCleaners())
        };
  };
  export default connect(mapStateToprops,mapDispatchToprops)(Welcome);
