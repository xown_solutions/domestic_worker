import React from "react";
import { Col } from "react-bootstrap";
import Card from "../../elements/Card/Card";


const Testimonial = () => {
  return (
    <Card className="p-4 bg-white testimonial text-center" style={{maxWidth: "316px"}}>
      <Col className="d-flex flex-wrap ">
        <p className="small text-center">
          I just love domestic workers. 
          I got a cleaner from them
          last month She has been prompt
          and diligent.
          No complains from me
        </p>
      </Col>

      <p className="search text-center">
        <span>~ &nbsp;</span>Name
        <span>&nbsp;~</span>
      </p>
    </Card>
  );
};
export default Testimonial;
