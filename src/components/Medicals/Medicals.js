import React from 'react';
import {  Row, Col, Form} from "react-bootstrap";
import Button from "../../elements/Button/Button";
import Card from "../../elements/Card/Card";

const Medicals = () => {
return(
  <>
   <Row className="space "><h6 className=" text-orange heading-01" >Your Medical Appointments</h6></Row> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">16 Aug, 2020</p>
    </Card> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">16 Aug, 2020</p>
    </Card> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Mon</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">16 Aug, 2020</p>
    </Card> 
    <Row className="space "><h6 className=" text-orange heading-01 mt-4" >Schedule A Check Up</h6></Row> 
    <Row className="space ">

<Form.Group as={Col} sm="4" controlId="paid medicals" className=" text-dark-grey">
              
                   <Form.Control
                     as="select"
                     name="paid medicals"
                     onChange=""
                     required
                     className="grey wallet"
                   
                   >
                      <option value="" >Paid Medicals</option>
                      <option value="" >Free</option>
                      </Form.Control>
                  
                      </Form.Group>   
                  
                      <Form.Group as={Col} sm="4" controlId="laboratory" className=" text-dark-grey">
              
                   <Form.Control
                     as="select"
                     name="laboratory"
                     onChange=""
                     required
                     className="grey wallet"
                   
                   >
                      <option value="" >Select Lab</option>
                      <option value="" >Afriglobal</option>
                      </Form.Control>
                  
                      </Form.Group> 
                      <Form.Group as={Col} sm="4" controlId="laboratory" className=" text-dark-grey">
              
              <Form.Control
                as="select"
                name="laboratory"
                onChange=""
                required
                className="grey wallet"
              
              >
                 <option value="" >Select Appointment</option>
                 <option value="" >8:00 am -12pm M0n -Fri</option>
                 <option value="" >12pm -2pm M0n -Fri</option>
                 </Form.Control>
             
                 </Form.Group> 
               
             </Row> 
             <Row className="space justify-content-end" >
     
    
     <Button className="  mr-4 mb-4 ">
     
       Submit
        
     
     </Button>
</Row>
<Row className="space "><h6 className=" text-orange heading-01" >Available specialized medicals</h6></Row> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">Basic chef medical check up</h6> <p  className="text-orange float-right ">₦6000</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> <p  className=" float-right "></p>
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">see details</p>
    </Card> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">Basic chef medical check up</h6> <p  className="text-orange float-right ">₦6000</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p>
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">see details</p>
    </Card> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">Basic chef medical check up</h6> <p  className="text-orange float-right ">₦6000</p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p>
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">see details</p>
    </Card> 
    <Row className="space "><h6 className=" text-orange heading-01" >Print Medical Report</h6></Row> 
    <Card className="p-4 mb-4" bank>
    <ul className="acc-card">
    <h6 className="text-dark-grey bold  pl-4">DW Starter Checkup </h6> <p  className="text-orange float-right ">print pdf </p>
    <li className="d-flex text-dark-grey">

    <p>Afriglobal, Ikeja </p> 
    </li>


    </ul>
    <p  className="text-dark-grey bold float-right ">Mon 16 Aug, 2020</p>
    </Card> 
  </>
)
}

export default Medicals