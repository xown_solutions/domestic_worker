import React ,{useEffect}from "react";
import {useHistory} from 'react-router';
import {connect} from 'react-redux';
import Card from "../../elements/Card/Card";
import Button from "../../elements/Button/Button";
import { Link } from "react-router-dom";
import { BsStarFill, BsStarHalf } from "react-icons/bs";
import { IconContext } from "react-icons";
import { Col} from "react-bootstrap";
import member from "../../assets/images/member/member3.jpg";
import { FaArrowRight } from "react-icons/fa";
import Member from "./Membe";

const FilteredMember = ({search}) => {
  const history=useHistory();
  /* useEffect(() => {
   
     if(!search.searchResult.length < 1 || search === ''){
  
      history.push('/')
     }
   
  }, [search])   */

 
  const DwList =search && search.searchResult &&search.searchResult.length ?( <>{
   search.searchResult.map((filtered,id) => (
   
        <Card className="member p-2 ml-2 mb-2"key={id}>
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
                {filtered.first_name} {filtered.last_name}{" "}
              </p>
              <p style={{ color: "#e77a1a" }}>{filtered.dw_type}</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "search" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
     
    ))}

        </>):search.searchResult.length  < 1 ?(<p className="mt-4 ">No result found</p> ):(<p><Member/> </p>) 
  
  return(
    <div className="d-flex flex-wrap mb-2  " >
     
    {DwList} 
    </div>
  );
};

const mapStateToprops = (state) => {
  return {
    dwData: state.displayDw,
   search: state.search,
  };
};


export default connect(mapStateToprops, null)(FilteredMember); 
