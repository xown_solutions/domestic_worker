import React, { useEffect } from "react";
import { connect } from "react-redux";
import { displayDW } from "../../redux/actions/displayDwAction";
import Card from "../../elements/Card/Card";
import Button from "../../elements/Button/Button";
import { Link } from "react-router-dom";
import { BsStarFill, BsStarHalf } from "react-icons/bs";
import { IconContext } from "react-icons";
import { Col} from "react-bootstrap";
import member from "../../assets/images/member/member3.jpg";
import { FaArrowRight } from "react-icons/fa";

const Member = ({ dwData, fetchDwData ,searchField}) => {
  useEffect(() => {
    fetchDwData();
  }, [fetchDwData]);
  
// eslint-disable-next-line no-unused-expressions

 
  return(
 <>
       <div className="d-flex flex-wrap mb-2 justify-content-center" >
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
               Olatunde Olutunrayo
              </p>
              <p style={{ color: "#e77a1a" }}>Nanny</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
               Olatunde Olutunrayo
              </p>
              <p style={{ color: "#e77a1a" }}>Nanny</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
               Olatunde Olutunrayo
              </p>
              <p style={{ color: "#e77a1a" }}>Nanny</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
               Olatunde Olutunrayo
              </p>
              <p style={{ color: "#e77a1a" }}>Nanny</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
        <Card className="member p-2 ml-2 mb-2">
          <div className="d-flex flex-wrap ">
            <div>
              <img
                src={member}
                alt="profile"
                style={{ borderRadius: 50 }}
                className="f-image"
              />
            </div>
            <div className="small p-2 ">
              <p>
                
               Olatunde Olutunrayo
              </p>
              <p style={{ color: "#e77a1a" }}>Nanny</p>
              <Button className="mt-2 ">
                <Link to="/profile:id" className="link small">
                  VIEW PROFILE
                </Link>
              </Button>
            </div>
          </div>

          <div>
            <p className="small">
              {" "}
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarFill />
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "small text-orange" }}>
                <BsStarHalf />
              </IconContext.Provider>
              &nbsp;<b>4.5/5</b>(5 jobs)
            </p>
          </div>
          <div className="verified  p-2">
            <button disabled className="small mb-2">
              ID VERIFIED
            </button>
            <button disabled className="small mb-2">
              REFREE VERIFIED
            </button>
            <button disabled className="small mb-2">
              GAURANTOR
            </button>
            <button disabled className="small mb-2">
              MEDICALS
            </button>
          </div>
        </Card>
        </div>
 </>
  );
};
const mapStateToprops = (state) => {
  return {
    dwData: state.displayDw,
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    fetchDwData: () => dispatch(displayDW()),
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(Member);
