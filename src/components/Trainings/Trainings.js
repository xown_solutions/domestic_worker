import React from 'react';
import {Dropdown} from "react-bootstrap";
import Profile from "../../assets/images/member/member1.jpg";


const Trainings = () => {
return(
  <>
     <div className="row row-cols-1 row-cols-sm-2 pb-3">
            <div className="col">
              <div className="pb-3"><h6 className="col_header">Welcome back Joshua,</h6></div>
              <div className="training-onwatch">
                  <img src={Profile} alt="profile" />
                  </div>
            </div>
            <div className="col">
              <div className="onwatch-text">You were watching</div>
            </div>
          </div>

          <div className="row row-cols-1 row-cols-sm-3 justify-content-between">
             <div className="col col-sm-7 pb-4"> <h6 className="col_header text-dark-grey">General training</h6><br/>
            <h6 className=" text-dark-grey bold">Choose from 20+ free online training to help you grow as a DW</h6></div> 
            
            <div className="col pb-4 ">
              <Dropdown >
                <label className="pr-2" style={{ color: "#F26527" }} >Select Language</label>  
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                  ENGLISH
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">
                    Another action
                  </Dropdown.Item>
                  <Dropdown.Item href="#/action-3">
                    Something else
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>

          <div className="row row-cols-1 row-cols-sm-3 pb-5">
            <div className="col">
            <div className="training-video">
                  <img src={Profile} alt="profile" />
                  </div>
                <div><p style={{ fontWeight:'800'}} className=" text-dark-grey">Managing Conflict in the Workplace</p></div>
                <div><p className=" text-dark-grey">Emokpare joshua</p></div>
            </div>
            <div className="col">
            <div className="training-video">
                  <img src={Profile} alt="profile" />
                  </div>
                <div><p style={{ fontWeight:'800'}} className=" text-dark-grey">Managing Conflict in the Workplace</p></div>
                <div><p className=" text-dark-grey">Emokpare joshua</p></div>
            </div>
            <div className="col">
            <div className="training-video">
                  <img src={Profile} alt="profile" />
                  </div>
                <div><p style={{ fontWeight:'800'}} className=" text-dark-grey">Managing Conflict in the Workplace</p></div>
                 <div><p className=" text-dark-grey">Emokpare joshua</p></div>
            </div>
          </div>
 <hr className="grey"/>
          <div className="row row-cols-1 row-cols-sm-3 justify-content-between ">
            
             <div className="col col-sm-7 pt-5"> <h6 className="col_header text-dark-grey">Professional trainings and Workshops</h6>
            <p className="text-dark-grey m-4">Learn in-house skills and get certifications upon completion!</p></div> 
            
            <div className="col pt-5">
              <Dropdown className="float-sm-right">
                <label className="pr-2" style={{ color: "#F26527" }} >Showing</label>  
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                  ALL
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">
                    Another action
                  </Dropdown.Item>
                  <Dropdown.Item href="#/action-3">
                    Something else
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          
          <div className="row row-cols-1 row-cols-sm-2 justify-content-between">

            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn float-sm-right  mob_image">SIGN UP</button>
            </div>
            
            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row pt-2 ">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn float-sm-right  mob_image">SIGN UP</button>
            </div>
            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn float-sm-right  mob_image">SIGN UP</button>
            </div>
            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn2 float-sm-right  mob_image">COMPLETED</button>
            </div>
            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn2 float-sm-right  mob_image">COMPLETED</button>
            </div>
            <div className="col pb-sm-3 border-y1">
                <div className="d-sm-flex flex-sm-row">
                  <div className="training-img">
                  <img src={Profile} alt="profile" />
                  </div>
                 
                    <div className="flex-col pl-3">
                        <div><p style={{ fontWeight:'800'}} className="text-dark-grey">DW Starter training</p></div>
                        <div><p className="text-dark-grey">Getting started as a domestic worker! Basic skill training, Personality management and work ethic.</p></div>
                        <div><p style={{ fontWeight:'500', color:'#F26527'}}>Avaliable</p></div>
                        <div><p style={{ fontSize:'10px'}} className="text-dark-grey">Domestic Workers</p></div>
                    </div>
                </div>
            </div>
            <div className="col border-y2">
                <button className="nbtn2 float-sm-right  mob_image">COMPLETED</button>
            </div>
          </div>  
  </>
)
}

export default Trainings