import React, { useState ,useRef} from "react";
import DWSignUp from "./DWSignUp";
import EngagerSignUp from "./EngagerSignUp";

import { Row, Tabs,Container, Col,Form } from "react-bootstrap";
import Carousel from "../Carousel/Slides";

import { Link } from "react-router-dom";
import logo from "../../assets/images/logo/logo.png"
import { FaTwitter, FaInstagram } from "react-icons/fa";
import { FiFacebook } from "react-icons/fi";
import { IconContext } from "react-icons";
import dateformat from "dateformat";

const SignUp = () => {
  const [key, setkey] = useState("domesticWorker");
   const {signUp} = useRef();  
   const today = new Date();
  return (
   < Container fluid className="reg-actions">
        <Row>
       
        <div className="auth-left col-xl-5 col-sm-12">
          <Col xs md={12} >
       
          <Col xs md={12}className=" d-flex flex-wrap justify-content-center" >
           <Link to="/">   <img
            alt="logo"
            src={logo}
            
            height="67"
            className="d-inline-block logo"
          /></Link> 
          </Col>
         
       < Col xs md={12} className="d-flex flex-wrap justify-content-center">
         
       <div id="signUp" className="border d-flex justify-content-center" ref={signUp} >
      <Row className="d-flex   justify-content-center">
        <Tabs
          id="signUp"
          activeKey={key}
          onSelect={(k) => setkey(k)}
          className=" nav-tabs col-8 d-flex justify-content-center signUp"
        >
          <div eventKey="domesticWorker" title="Domestic Worker"  >
            <DWSignUp />
          </div>
          <div eventKey="engager" title="Engager"  >
            <EngagerSignUp />
          </div>
        </Tabs>
      </Row>
    </div>
              </Col>
              <Col className="p-2 mb-2 text-center " xs xl={12}>
              <Link
                to="https://instagram.com/xown_solution/"
                target="_blank"
                aria-label="Instagram"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FaInstagram />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://twitter.com/xown_solution/"
                target="_blank"
                aria-label="twitter"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FaTwitter />
                </IconContext.Provider>
              </Link>
              <Link
                to="https://facebook.com/XownSolutionsLimited/"
                target="_blank"
                aria-label="Facebook"
                className="pr-3"
              >
                <IconContext.Provider value={{ className: "icons grey-color" }}>
                  <FiFacebook />
                </IconContext.Provider>
              </Link>
            </Col>
            <Col className="p-2 mb-2 text-center ">
              <p className="small text-orange">
                &copy; {dateformat(today, "yyyy")}
                <Link to="/"  className=" text-orange">&nbsp;Domestic Workers</Link>
              </p>
            </Col>
          </Col>
          </div>
          <Col xs={12} xl={7} className="hide">
            <Carousel/>
          </Col>
        </Row>
       
         
      </Container>
   
  );
};

export default SignUp;
