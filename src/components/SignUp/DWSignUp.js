import React, { Component } from "react";
import { connect } from "react-redux";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { submitDW } from "../../redux/actions/dwRegAction";
import { fetchCategories } from "../../redux/actions/fetchCategoriesAction";
import { fetchIdentification } from "../../redux/actions/fetchIdentificationAction";
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";

import { Container, Col,Form } from "react-bootstrap";
import { Link } from "react-router-dom";

toast.configure()

class DWSignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
     
      first_name: "",
      surname: "",
      email: "",
      mobile: "",
      location: "",
      password: "",
      confirmPassword: "",
      identity_type_id: "",
      identity_no: "",
      dw_type_id: "",
      role_id:1,
      validated: false,
      error: {
        first_name: "",
        surname: "",
        email: "",
        mobile: "",
        location: "",
        password: "",
        confirmPassword: "",
        identity_type_id: "",
        identity_no: "",
        dw_type_id: "",
      },
    };
    this.domesticWorker=React.createRef()
  }
  
  componentDidMount() {
    this.props.fetchCategories();
    this.props.fetchIdentification();
  }
  validateForm = ({ error, ...rest }) => {
    let isValid = true;

    Object.values(error).forEach((val) => {
      val.length > 0 && (isValid = false);
    });
    Object.values(rest).forEach((val) => {
      if (val === null) {
        isValid = false;
      } else {
        isValid = true;
      }
      console.log(this.props);
    });

    return isValid;
  };
  
  //^[+]*+[(]{0,2}[0-9]{1,5}[)]{0,1}[-\s\./0-9]*$/
  handleChange = (event) => {
    const emailregExp = RegExp(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    );
    const phoneregExp = RegExp(/(^[0]\d{10}$)|(^[+]?[234]\d{12}$)/);
    const passwordregExp= RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/);
    const { name, value } = event.target;
    let error = { ...this.state.error };
    const { password} = this.state;
    switch (name) {
      
      case "first_name":
        error.firstname =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "surname":
        error.surname =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "location":
        error.location =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "mobile":
        error.mobile = phoneregExp.test(value) ? "" : "phone number is invalid";
        break;
      case "identity_type_id":
        error.id = value.length < 1 ? "Field cannot be empty" : "";
        break;
      case "identity_no":
        error.identity_no = value.length < 4 ? "Provide a valid Number" : "";
        break;
      case "dw_type_id":
        error.dw_type_id = value.length < 1 ? "Field can not be empty" : "";
        break;
      case "email":
        error.email = emailregExp.test(value) ? "" : "Email address is invalid";
        break;
      case "password":
        error.password =
           passwordregExp.test(value) ? "":"password must be minimum of 8 characters,contain at least a numeric digit,  uppercase and lowercase letter" ;
        break;
       case "confirmPassword":
        error.confirmPassword =
         value === password ? "" : "password does not match";
        break;
      default:
        break;
    }
  
    this.setState(
      {
        [event.target.name]: event.target.value,
        error,
      },
      () => console.log(this.state)
    );
   
  };

  handleSubmit = (event) => {
    event.preventDefault();
   
    const form = event.currentTarget;
   
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();

      toast.error("Can not submit empty data",{postion:toast.POSITION.TOP_CENTER});
      this.state.error.password.length > 0 && toast.error(this.state.error.password,{postion:toast.POSITION.TOP_CENTER});
    } else if (this.validateForm(this.state)) {
      console.info("Valid Form");
      this.setState({ ...this.state, validated: true});
      /* this.props.submitDW(this.state); */

      window.location="/dashboard"  
    } 
  };

  render() {
 
    const { error } = this.state;
    if(this.props.dwReg.success.message){
     
      toast.success("sucessfully Registered",{postion:toast.POSITION.TOP_CENTER,autoclose:8000});
      delete   this.props.dwReg.success.message;
  /* window.location="/dashboard"  */ 
  }  
    else if (this.props.dwReg.error ){ 
      Object.keys(this.props.dwReg.error).map((errorMsg)=>(

        this.props.dwReg.error[errorMsg].map((msg)=>{
        toast.error(msg,{postion:toast.POSITION.TOP_CENTER});
          delete   this.props.dwReg.error[errorMsg];
          
         })
  
      ))  
       
  } 
   
    return (
      
      <Container  id="domesticWorker" ref={this.domesticWorker}>
       
        <div className="d-flex  justify-content-center text-center col-12">
   
        
            <Form
              noValidate
              onSubmit={this.handleSubmit}
           
              validated={this.state.validated}
              >
          
              <p style={{color:"red"}}>{this.state.msg}</p>
              <Form.Row>
                <Form.Group as={Col} col="6" controlId="first_name">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="First name *"
                      name="first_name"
                      onChange={this.handleChange}
                      min="4"
                      required
                    />
                  </Input>
                  {error.first_name.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.first_name}!!
                    </span>
                  )}
                </Form.Group>
                <Form.Group as={Col} col="6" controlId="surname">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Surname *"
                      name="surname"
                      min="4"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.surname.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.surname}!!
                    </span>
                  )}
                </Form.Group>

                <Form.Group as={Col} sm="12">
                  <Input>
                    <Form.Control
                      type="email"
                      placeholder="Email *"
                      pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-z]{2,4})$"
                      name="email"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.email.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.email}!!
                    </span>
                  )}
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} sm="12" controlId="mobile">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Mobile no *"
                      name="mobile"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.mobile.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.mobile}!!
                    </span>
                  )}
                </Form.Group>
                <Form.Group as={Col} sm="12" controlId="location">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Location *"
                      name="location"
                      min="4"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.location.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.location}!!
                    </span>
                  )}
                </Form.Group>
                
                </Form.Row>
                <Form.Row>
                <Form.Group as={Col} col="6" controlId="password">
                  <Input>
                    <Form.Control
                      type="password"
                      placeholder="Password *"
                      name="password"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  
               
                </Form.Group>
                <Form.Group as={Col} col="6">
                  <Input>
                    <Form.Control
                      type="password"
                      placeholder="Confirm Password *"
                      name="confirmPassword"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>

                  {error.confirmPassword.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.confirmPassword}!!
                    </span>
                  )}
                </Form.Group>
                </Form.Row>
            {/*   <Form.Group as={Col} sm="12" controlId="category">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Enter Your Preferred Job Type *"
                      name="category"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.mobile.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.mobile}!!
                    </span>
                  )}
                </Form.Group>
                */}

             
                  <Form.Row >
                    <Form.Group as={Col} sm="12" controlId="dw_type_id">
                      <Input>
                        <Form.Control
                          as="select"
                          name="dw_type_id"
                          onChange={this.handleChange}
                          required
                        >
                          <option value="" >Choose Preferred Job Type</option>
                          {this.props.categories &&
                this.props.categories.categories &&
                this.props.categories.categories.map((category) => (
                          <option value={category.id} key={category.id}>
                            {category.name}
                          </option>
                ))}
                        </Form.Control>
                      </Input>
                      {error.dw_type_id.length > 0 && (
                        <span className="form -control-feedback invalid">
                          {error.dw_type_id}!!
                        </span>
                      )}
                    </Form.Group>
                  </Form.Row>
               
 
              <Form.Row>
    
                <Form.Group as={Col} sm="6" controlId="identity_type_id">
                  <Input>
                 
                     
                    <Form.Control
                      as="select"
                      name="identity_type_id"
                      onChange={this.handleChange}
                      required
                      > <option value="">Identification *</option>
                         {this.props.identification &&
                      this.props.identification.identification &&
                      this.props.identification.identification.map((identification) => (
                      <option value={identification.id} key={identification.id} > 
                      {identification.name}
                          </option>
                      ) )}
                    </Form.Control>
                  
                  </Input>
                  
                  {error.identity_type_id.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.identity_type_id}!!
                    </span>
                  )}
                </Form.Group>
              
                <Form.Group as={Col} sm="6" controlId="identity_no">
                  <Input>
                    <Form.Control
                      required
                      type="text"
                      placeholder="Id No*"
                      name="identity_no"
                      onChange={this.handleChange}
                    />
                  </Input>
                  {error.identity_no.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.identity_no}!!
                    </span>
                  )}
                </Form.Group>
              </Form.Row>
               
              <Form.Row className="d-flex justify-content-center">
              <Button type="submit"name="submit" disabled={this.props.dwReg.loading }>{this.props.dwReg.loading ? "...loading" : "Sign Up"}</Button>
              </Form.Row>
              <p tyle={{ color: "#707070" }} className="text-center">
                Already have an account? <Link to="/signIn" className=" text-orange">Sign in</Link>
              </p>
            </Form>
       
        </div>
      </Container>
    );
  }
}
const mapStateToprops = (state) => {
  return {
    categories: state.fetchCategories,
    identification: state.fetchIdentification,
    dwReg:state.dwReg
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    submitDW: (users) => dispatch(submitDW(users)),
    fetchCategories: () => dispatch(fetchCategories()),
    fetchIdentification: () => dispatch(fetchIdentification()),
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(DWSignUp);
