import React, { Component } from "react";
import { connect } from "react-redux";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { submitEngager } from "../../redux/actions/engagerRegAction";
import { fetchEngagerCategories} from "../../redux/actions/fetchCategoriesAction";
import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button";

import { Container, Col,Form } from "react-bootstrap";
import { Link } from "react-router-dom";

toast.configure()

class EngagerSignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: "",
      surname: "",
      email: "",
      mobile: "",
      location: "",
      password: "",
      confirmPassword: "",
      engager_type_id: "",
      role_id:2,
      validated: false,
      company_name: "",
      cac_number: "",
      company_address:"",
      error: {
        first_name: "",
        surname: "",
        email: "",
        mobile: "",
        location: "",
        password: "",
        company_address:"",
        confirmPassword: "",
        engager_type_id: "",
        company_name: "",
        cac_number: "",
      },
    };

    this.engager=React.createRef()
    this.hire=React.createRef()
  }
  
  componentDidMount() {
    this.props.fetchEngagerCategories();
   
  
  }
 
  validateForm = ({ error, ...rest }) => {
    let isValid = true;

    Object.values(error).forEach((val) => {
      val.length > 0 && (isValid = false);
    });
    Object.values(rest).forEach((val) => {
      if (val === null) {
        isValid = false;
      } else {
        isValid = true;
      }
      console.log(this.props);
    });

    return isValid;
  };
  
  //^[+]*+[(]{0,2}[0-9]{1,5}[)]{0,1}[-\s\./0-9]*$/
  toggle= ()=>{
   
    if (this.state.engager_type_id === "1") {
      this.hire.current.style.display =  'none';
    }
    else {
      this.hire.current.style.display ='flex'
    }
}
  handleChange = (event) => {
    const emailregExp = RegExp(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    );
    const phoneregExp = RegExp(/(^[0]\d{10}$)|(^[+]?[234]\d{12}$)/);
    const passwordregExp= RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/);
    const { name, value } = event.target;
    let error = { ...this.state.error };
    const { password} = this.state;
    switch (name) {
      
      case "first_name":
        error.firstname =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "surname":
        error.surname =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "location":
        error.location =
          value.length < 4 ? "Atleast 4 characaters required" : "";
        break;
      case "mobile":
        error.mobile = phoneregExp.test(value) ? "" : "phone number is invalid";
        break;
     
      case "engager_type_id":
        error.engager_type_id = value.length < 1 ? "Field can not be empty" : "";
        break;
      case "email":
        error.email = emailregExp.test(value) ? "" : "Email address is invalid";
        break;
      case "password":
        error.password =
           passwordregExp.test(value) ? "":"password must be minimum of 8 characters,contain at least a numeric digit,  uppercase and lowercase letter" ;
        break;
       case "confirmPassword":
        error.confirmPassword =
         value === password ? "" : "password does not match";
        break;
        case "company_address":
          error.company_address =
            value.length < 4 ? "Atleast 4 characaters required" : "";
          break;
         
        case "company_name":
          error.company_name =
            value.length < 4 ? "Atleast 4 characaters required" : "";
          break;
        case "cac_number":
          error.cac_number=
            value.length < 4 ? "A valid CAC is required" : "";
          break;
        
      default:
        break;
    }
  
    this.setState(
      {
        [event.target.name]: event.target.value,
        error,
      },
      () => { 
       this.toggle()
      console.log(this.state)
    }
    );
   
  };
 
  handleSubmit = (event) => {
   
    event.preventDefault();
    const form = event.currentTarget;
   
    
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();

      toast.error("Can not submit empty data",{postion:toast.POSITION.TOP_CENTER});
      this.state.error.password.length > 0 && toast.error(this.state.error.password,{postion:toast.POSITION.TOP_CENTER});
    } else if(this.state.engager_type_id === "2" && (this.state.company_name === "" || this.state.cac_number === "" || this.state.company_address ==="")){
      toast.error("Company name,address And CAC required for organizations",{postion:toast.POSITION.TOP_CENTER});
    }else if (this.validateForm(this.state) ) {
      this.setState({ ...this.state, validated: true});
     /*  this.props.submitEngager(this.state) */
     window.location="/dashboard"  
   
  }
  
  };

  render() {
 
    const { error } = this.state;
  if(this.props.engagerReg.success.message){
     
      toast.success("sucessfully Registered",{postion:toast.POSITION.TOP_CENTER,autoclose:8000});
      delete   this.props.engagerReg.success.message;
//  window.location="/dashboard"  
  }  
    else if (this.props.engagerReg.error ){ 
      
      Object.keys(this.props.engagerReg.error).map((errorMsg)=>(

        this.props.engagerReg.error[errorMsg].map((msg)=>{
        toast.error(msg,{postion:toast.POSITION.TOP_CENTER});
          delete   this.props.engagerReg.error[errorMsg];
         })
  
      )) 
       
  } 

    return (
      
      <Container  id="domesticWorker" ref={this.engager} >
      
        <div className="d-flex  justify-content-center text-center col-12">

            <Form
              noValidate
              onSubmit={this.handleSubmit}
              className="bg-white"
              validated={this.state.validated}
              >
          
              <Form.Row>
                <Form.Group as={Col} col="6" >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="First name *"
                      name="first_name"
                      onChange={this.handleChange}
                      min="4"
                      required
                    />
                  </Input>
                  {error.first_name.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.first_name}!!
                    </span>
                  )}
                </Form.Group>
                <Form.Group as={Col} col="6" >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Surname *"
                      name="surname"
                      min="4"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.surname.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.surname}!!
                    </span>
                  )}
                </Form.Group>
              </Form.Row>
              <Form.Row >
                    <Form.Group as={Col} sm="12" controlId="engager_type_id">
                      <Input>
                        <Form.Control
                          as="select"
                          name="engager_type_id"
                          onChange={this.handleChange}
                          required
                        >
                           <option value="" >Hire  as</option>
                          {this.props.categories &&
                this.props.categories.engagerCategories &&
                this.props.categories.engagerCategories.map((engagerCategory) => (
                          <option value={engagerCategory.id} key={engagerCategory.id}>
                            {engagerCategory.name}
                          </option>
                ))}
                         
                          {/* <option value="Individual" >Individual</option>
                          <option value="Co-operate" >Co-operate</option> */}
                          </Form.Control>
                          </Input> 
                          {error.engager_type_id.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.engager_type_id}!!
                    </span>
                  )}
                          </Form.Group>
                  </Form.Row>
                  <Form.Row style={{display:"none"}} ref={this.hire}>
               
                <Form.Group as={Col} col="6"  controlId="company_name" >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Company Name *"
                      name="company_name"
                      onChange={this.handleChange}
                      
                    />
                  </Input>
                  
                  {error.company_name.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.company_name}!!
                    </span>
                  )}
                </Form.Group>
                <Form.Group as={Col} col="6">
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Company CAC *"
                      name="cac_number"
                      onChange={this.handleChange}
                     
                    />
                  </Input>

                  {error.cac_number.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.cac_number}!!
                    </span>
                  )}
                </Form.Group>
               
                  <Form.Group as={Col} sm="12" col={12} >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Company's Address *"
                      name="company_address"
                      min="4"
                      onChange={this.handleChange}
                    
                    />
                  </Input>
                  {error.company_address.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.company_address}!!
                    </span>
                  )}
                </Form.Group>
                </Form.Row>
         
                
              <Form.Row>
                <Form.Group as={Col} sm="12">
                  <Input>
                    <Form.Control
                      type="email"
                      placeholder="Email *"
                      pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-z]{2,4})$"
                      name="email"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.email.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.email}!!
                    </span>
                  )}
                </Form.Group>
                </Form.Row>
              <Form.Row>
                <Form.Group as={Col} sm="12" >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Mobile no *"
                      name="mobile"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.mobile.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.mobile}!!
                    </span>
                  )}
                </Form.Group>
                <Form.Group as={Col} sm="12" >
                  <Input>
                    <Form.Control
                      type="text"
                      placeholder="Location *"
                      name="location"
                      min="4"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  {error.location.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.location}!!
                    </span>
                  )}
                </Form.Group>
                
                </Form.Row>
                <Form.Row>
                <Form.Group as={Col} col="6" >
                  <Input>
                    <Form.Control
                      type="password"
                      placeholder="Password *"
                      name="password"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>
                  
               
                </Form.Group>
                <Form.Group as={Col} col="6">
                  <Input>
                    <Form.Control
                      type="password"
                      placeholder="Confirm Password *"
                      name="confirmPassword"
                      onChange={this.handleChange}
                      required
                    />
                  </Input>

                  {error.confirmPassword.length > 0 && (
                    <span className="form -control-feedback invalid">
                      {error.confirmPassword}!!
                    </span>
                  )}
                </Form.Group>
                </Form.Row>   
                
              <Form.Row className="d-flex justify-content-center">
                <Button type="submit"name="submit" disabled={this.props.engagerReg.loading }>{this.props.engagerReg.loading ? "...loading" : "Sign Up"}</Button>
              </Form.Row>
              <p tyle={{ color: "#707070" }} className="text-center">
                Already have an account? <Link to="/signIn" className=" text-orange">Sign in</Link>
              </p>
            </Form>
       
        </div>
      </Container>
    );
  }
}
const mapStateToprops = (state) => {
  return {
    categories: state.fetchCategories,
    engagerReg:state.engagerReg
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    submitEngager: (engagers) => dispatch(submitEngager(engagers)),
    fetchEngagerCategories: () => dispatch(fetchEngagerCategories())
    
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(EngagerSignUp);
