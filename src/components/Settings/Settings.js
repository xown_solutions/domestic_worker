import React from 'react';
import { Link } from "react-router-dom";
import {  Row, Col, Form} from "react-bootstrap";
import Card from "../../elements/Card/Card";
import Button from "../../elements/Button/Button";

import Profile from "../../assets/images/member/member1.jpg";
import './settings.css';

const Settings = () => {
return(
  
        <>
     
        <Form  className="settings">
        <div className="mb-3"><h6 className="col_header">Basic Data</h6></div>
          <Row>
            <Col  md={6}>
              <Form.Group controlId="formFirstName">
                <Form.Label className="label_text">First Name</Form.Label>
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                />
              </Form.Group>
              <Form.Group controlId="formLastName">
                <Form.Label className="label_text">Last Name</Form.Label>
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                />
              </Form.Group>
              <Form.Group controlId="formEmail">
                <Form.Label className="label_text">Email Address</Form.Label>
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                />
              </Form.Group>
              <Form.Group controlId="formPhone">
                <Form.Label className="label_text">Phone Number 1</Form.Label>
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                  placeholder="08123456789"
                />
              </Form.Group>
              <Form.Group controlId="formPhone">
                <Form.Label className="label_text">Phone Number 2</Form.Label>
         
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                />
               <p className="text-orange "> Add New</p>
         
              </Form.Group>
              <Form.Group controlId="formHomeAdd">
                <Form.Label className="label_text">Home Address</Form.Label>
                <Form.Control
                  className="input_container"
                  type="text"
                  disabled
                />
                 {/*hide if domestic worker*/}<p className="text-orange"> Modify</p>
              </Form.Group>
            </Col>
            <Col md={6}>
             
                  <div className="profile-image">   <img src={Profile} alt="profile" /></div>
             
                <div className="file_upload_button d-flex flex-wrap justify-content-center">
                  <label className="file_upload_label text-orange"><input type="file" style={{display: "none"}}/>Tap to Change</label>
                </div>
                <div className="d-flex flex-wrap justify-content-center">
            
                  <label for="custom-switch">Avaliable For Jobs On Demand</label>
                  <Form.Check 
                type="switch"
                id="custom-switch"
                label=""
                    />
                </div>
           
            </Col>
          </Row>
         
            <p className="  mx-auto row desc_label mt-2">You can change your picture and the 'On Demand' notification with this form. To change your account information please send an email to sales@domesticworker.com</p>
            <div className="d-flex  justify-content-center">
            <Button className="  mt-4 mb-4  pb-4 pt-4" style={{textTransform:"uppercase"}}>
           
                  {" "}
                  Update Profile
                 
            
              </Button>
              </div>
            
        </Form>

        {/* if category is enagegr hide content below*/}
      <hr className="grey"/>
        <Form  className="div_container_left">
        <div className="mb-3"><h6 className="col_header">ACCOUNT SETTING</h6></div>
      
      <Row>
            {/* <Col className=" col-sm-12 col-md-6  mb-4"> */}
            <div  className=" col-sm-12 col-md-6  mb-4">
              <h6  className=" text-dark-grey m-4 heading col-sm-12 col-md-6  mb-4"> Account 1</h6>
              <Card className="pt-4 pb-4" bank>
                <ul className="acc-card">
                <h6 className="text-dark-grey bold pl-4">UBA</h6> <p  className="text-orange float-right ">Default</p>
                <li className="d-flex text-dark-grey">
                
                 <h2>Emokpare Joshua   </h2>  
                 </li>
               

              </ul>
            
            </Card>
            </div>
              {/*   </Col>
              <Col className=" col-sm-12 col-md-6 mb-4"> */}
                 <div  className=" col-sm-12 col-md-6  mb-4">
              <h6  className="text-dark-grey heading m-4 col-sm-12 col-md-6  mb-4"> Account 2</h6>
              <Card  className="pt-4 pb-4" bank>
              <ul className="acc-card">
             
              <h6 className="text-dark-grey bold pl-4">Access</h6>
              <li className="d-flex text-dark-grey">
                
                <h2>Emokpare Joshua   </h2>  
                </li>
          
           </ul>
           </Card>
           </div>
          {/*   </Col> */}
    </Row>
   <Row>
     
     <Button className="acc-btn mr-4 mb-4  
     ">
                <Link to="/account">
                  {" "}
                   ADD ACCOUNT
                 
                </Link>
              </Button>
              <Button className="acc-btn   mb-4 
              " grey>
                <Link to="/remove-account">
                  {" "}
               REMOVE ACCOUNT
                 
                </Link>
              </Button>
     </Row>
     <Row className="space settings-select" >
     
     <Col sm={12} md={6}>
     <div className="col-12  wallet-button mr-4 "><h6 className="col_header">Preferred Language</h6></div>
     
     <Form.Group as={Col}  controlId="language" >
                   
                        <Form.Control
                          as="select"
                          name="language"
                          onChange=""
                          required
                          className="  text-grey"
                        
                        >
                           <option value="" >Select Language</option>
                           <option value="" >English</option>
                           </Form.Control>
                       
 </Form.Group>   
      </Col>
      <Col sm={12} md={6}>
     <div className="  col-12 wallet-button mr-4 "><h6 className="col_header">DW Upgrade</h6></div>
     <Form.Group as={Col} controlId="account-upgrade">
                   
                   <Form.Control
                     as="select"
                     name="account-upgrade"
                     onChange=""
                     required
                     className="  text-grey"
                   
                   >
                      <option value="" >Upgrade Type</option>
                      <option value="" >DW starter</option>
                      </Form.Control>
                  
</Form.Group>   
      </Col>
            
     </Row>
        
          <p className="mx-auto row desc_label pt-3 text-grey" style={{ display: 'block'}}>Your changes will be effective after you confirm the account.</p>
          <Row className="space justify-content-center" >
     
     <Button className="  mr-4 mb-4 p-4 mt-4">
                <Link to="/account">
                  {" "}
                 SAVE CHANGES
                 
                </Link>
              </Button>
         
     </Row>
        </Form>
      </>
  
)
}

export default Settings