import React ,{Component} from 'react'
import {  Table,Tag, Space,Col  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
//const { Column, ColumnGroup } = Table;

const columns = [
    {
      title: 'Lab ID',
      dataIndex: 'labId',
         // specify the condition of filtering result
      // here is that finding the labId started with `value`
      onFilter: (value, record) => record.labId.indexOf(value) === 0,
      sorter: (a, b) => a.labId.length - b.labId.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'Name',
      dataIndex: 'name',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Details',
      dataIndex: 'details',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.details.length - b.details.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Price',
        dataIndex: 'price',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.price.length - b.price.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      labId: `LBOOO${i}`,
      name: `Basic Chef Checkup ${i}`,
      details: `covers Hypertesis B test,Blood type ${i}`,
     price: 1500 + i,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class SpecializedMedicals extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>
          <h5 className="text-dark-grey heading-06" >SPECIALISED MEDICALS</h5>
        <div className="row ">

        
</div>
<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>
<Col  className="col-sm-12 col-md-6  mb-2" style={{display:'flex'}}>
        <Button type="submit"name="submit" className=" mr-2" >ADD NEW</Button>
<Button type="submit"name="submit"  grey>DELETE MULTIPLE</Button>
</Col>

</>
        )
}
  
}


export default SpecializedMedicals