import React ,{Component} from 'react'
import {  Table,Tag, Space,Col  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
//const { Column, ColumnGroup } = Table;

const columns = [
    {
      user :'User ID',
      dataIndex: 'userId',
         // specify the condition of filtering result
      // here is that finding the userId started with `value`
      onFilter: (value, record) => record.userId.indexOf(value) === 0,
      sorter: (a, b) => a.userId.length - b.userId.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'User Name',
      dataIndex: 'name',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Payment Type',
      dataIndex: 'paymentType',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.paymentType.length - b.paymentType.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Description',
        dataIndex: 'description',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.description.length - b.description.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      user: `DW00${i}`,
      name: `Adeyemo Adeyinka ${i}`,
      paymentType: `credit ${i}`,
     description: `Dw Upgrade ${i}`,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class PaymentHistory extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>
          <h5 className="text-dark-grey heading-06" >PAYMENT HISTORY</h5>
       
<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>

</>
        )
}
  
}


export default PaymentHistory