import React from 'react';
import {  Row} from "react-bootstrap";
import Card from "../../../elements/Card/Card";
import Button from "../../../elements/Button/Button";

const Billings = () => {
  return (
<>
   <Row className="space "><h6 className=" text-orange heading-01" >Billings</h6>
   <h5 className=" text-grey col-12 " >Here you can view payments and subscriptions made </h5>
   </Row> 

   <Row className="space "><h6 className=" text-dark-grey heading-06" >PRODUCT PACKS</h6></Row>
   <Row >
    <Card className="p-4 m-2" billing>
    <ul className="billing-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">Free</p>
    <li className="d-flex text-dark-grey">

    </li>
    </ul>
   
    </Card> 
    <div style={{position:'relative'}}>
    <Button grey style={{position:"absolute",bottom:3}}>
     
     Modify
 
    </Button>
    </div>

    </Row> 
    <Row >
    <Card className="p-4 m-2" billing>
    <ul className="billing-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">₦6,000</p>
    <li className="d-flex text-dark-grey">

    </li>
    </ul>
   
    </Card> 
    <div style={{position:'relative'}}>
    <Button grey style={{position:"absolute",bottom:3}}>
     
     Modify
 
    </Button>
    </div>
    </Row >
    <Row >
    <Card className="p-4 m-2" billing>
    <ul className="billing-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">₦15,000</p>
    <li className="d-flex text-dark-grey">

    </li>
    </ul>
   
    </Card> 
    <div style={{position:'relative'}}>
    <Button grey style={{position:"absolute",bottom:3}}>
     
     Modify
 
    </Button>
    </div>
    </Row >
    <Row >
    <Card className="p-4 m-2" billing>
    <ul className="billing-card">
    <h6 className="text-dark-grey  pl-4">DW Starter Check Up</h6> <p  className="text-orange float-right ">₦60,000</p>
    <li className="d-flex text-dark-grey">

    </li>
    </ul>
   
    </Card>
    <div style={{position:'relative'}}>
    <Button grey style={{position:"absolute",bottom:3}}>
     
     Modify
 
    </Button>
    </div>
    </Row >  
    <Row className="space justify-content-start" >
     
    
     <Button className="   m-2 ">
     
       Add New
        
     
     </Button>
</Row>
    </>
  )
}



export default Billings