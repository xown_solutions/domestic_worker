import React from 'react'
import {  Col,Form,Input,Row }from 'antd';
import { FaPen} from "react-icons/fa";
import { IconContext } from "react-icons";
import "./Settings.css";

const Ratings = () => {
  return (
    
    <Form
    name="filter"
className="flex  rating row filter"
gutter={[16, 16]}
  >
  <div className="mt-4 mb-4">
  <h5 className="text-dark-grey heading-06 mt-4 " >RATING CRITERIA</h5>
<h5 className="pl-0 text-dark-grey col-12 " >Set up rating system for DW.</h5>
  </div>

<Col className="col-12 maxRate"> 
   <Form.Item name="maxRating" label="Maximum Rating Score" className=" text-orange heading-06" >
     <Input />
     <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
    </Form.Item> 
    </Col>
    <Col className="col-12">

   <Form.Item name="cumRating" label="Maximum cumulative engager ratings score"className="  heading-06" >
    
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
   </Form.Item>
    
   <Form.Item name="calRating" label="Calculate from last" >
    
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
   </Form.Item>
     
      </Col>
  
  <Col className="col-sm-6 col-md-6 verify">
 <h5 className="text-dark-grey heading-06" >Verifications </h5>
 
 <Form.Item name="idVerification" label="Id Verification" >
     <div className="float">
    
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
                 
               </IconContext.Provider>
    </div> 
   </Form.Item>
     
   <Form.Item name="addressVerification" label="Address Verification" >
     <div className="float">
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
                 
               </IconContext.Provider>
               </div>
   </Form.Item>
     
   <Form.Item name="guarantorVerification" label="Guarantor Verification" >
     <div className="float">
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
               </div> 
   </Form.Item>
     
   <Form.Item name="employmentHistory" label="Employment History" >
     <div className="float">
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
               </div> 
   </Form.Item>
     
   <Form.Item name="medicals" label="Medicals" >
     <div className="float">
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
               </IconContext.Provider>
               </div> 
   </Form.Item>
  </Col>
  <Col className="col-sm-6 col-md-6">
  <Form.Item name="trainings" label="Trainings" >
  <div className="float">
    <Input />
    <IconContext.Provider value={{ className: "icons text-dark-grey" }}>
                 <FaPen/> 
   </IconContext.Provider>
   </div> 
   </Form.Item>
  </Col>
  </Form>
    
      
  )
}



export default Ratings