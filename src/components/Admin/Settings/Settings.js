import React from 'react'
import {  Row} from "react-bootstrap";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
import Card from "../../../elements/Card/Card";
import Ratings from  "./RatingCiteria";
import Button from "../../../elements/Button/Button";

const Settings = () => {
  return (

    <div className="pl-40 pl-10">
    
           <Row className="space "><h6 className=" text-orange heading-01" >General Settings</h6></Row> 
           <Row className="space "><h6 className=" text-dark-grey heading-06" >DW JOB TYPE</h6></Row>
           <Row >
    <Card className=" m-2" pills>
   
    <h6 className="text-white p-2 ">HOUSEHELP<p  className="text-orange float-right mr-2"> 
    <IconContext.Provider value={{ className: "icons" }}>
                  <FaPen/> <FaTrash/>
                </IconContext.Provider></p>
    
                </h6> 
   
    </Card> 
    <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">CHEF<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">DRIVER<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">NANNY<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2  ">GARDENER<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2  ">GATEMAN<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2  ">HOUSEHELP<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">LAUNDARY MAN<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
    </Row> 
    <Row className="space justify-content-start" >
     
    
     <Button className="   m-2 ">
     
       Add +
        
     
     </Button>
</Row>
<Row className="space "><h6 className=" text-dark-grey heading-06" >ENGAGER TYPE</h6></Row>
           <Row >
    <Card className=" m-2" pills>
   
    <h6 className="text-white p-2 ">COMPANY<p  className="text-orange float-right mr-2"> 
    <IconContext.Provider value={{ className: "icons" }}>
                  <FaPen/> <FaTrash/>
                </IconContext.Provider></p>
    
                </h6> 
   
    </Card> 
    <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">AGENCY<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
   <Card className=" m-2" pills>
   
   <h6 className="text-white p-2 ">INDIVIDUAL<p  className="text-orange float-right mr-2"> 
   <IconContext.Provider value={{ className: "icons" }}>
                 <FaPen/> <FaTrash/>
               </IconContext.Provider></p>
   
               </h6> 
  
   </Card> 
  
    </Row> 
    <Row className="space justify-content-start" >
     
    
     <Button className="   m-2 ">
     
       Add +
        
     
     </Button>
</Row>
<Ratings/>
           </div>
  )
}



export default Settings