import React ,{Component} from 'react'
import {  Table,Tag, Space,Col,Form,Select,Input  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
import './Medicals.css';
//const { Column, ColumnGroup } = Table;
const { Option } = Select;

const columns = [
    {
      title: 'DW ID',
      dataIndex: 'dwId',
         // specify the condition of filtering result
      // here is that finding the dwId started with `value`
      onFilter: (value, record) => record.dwId.indexOf(value) === 0,
      sorter: (a, b) => a.dwId.length - b.dwId.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'Check-up Type',
      dataIndex: 'checkup',
       onFilter: (value, record) => record.checkup.indexOf(value) === 0,
      sorter: (a, b) => a.checkup.length - b.checkup.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Lab Name',
      dataIndex: 'labName',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.labName.length - b.labName.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Time',
        dataIndex: 'time',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.time.length - b.time.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      dwId: `DW${i}`,
      checkup: `Medical Plan 1 ${i}`,
      labName: `St. Tomas Hospital. ${i}`,
     time: `4 :0`+ 3* i ,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class ScheduledAppointments extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>
          <h5 className="text-dark-grey heading-06 mt-4 mb-4" >SCHEDULED APPOINTMENTS</h5>
        
 <Form
      name="filter"
  className="flex medic row filter"
  gutter={[16, 16]}
    >
      
   <Col  className="col-sm-6 col-md-3">
      <Form.Item name="checkup-type" label="Check Up Type " >
         <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="">Basic Check up 1</Option>
          
        </Select>
      </Form.Item>
        </Col>
    
    
     
    </Form>
      
        

<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>
<Col  className="col-sm-12 col-md-6  mb-2" style={{display:'flex'}}>
        <Button type="submit"name="submit" className=" mr-2 mb-2" >ADD NEW APPOINTMENTS</Button>
<Button type="submit"name="submit" className=" mb-2"  grey>DELETE MULTIPLE</Button>
</Col>

</>
        )
}
  
}


export default ScheduledAppointments