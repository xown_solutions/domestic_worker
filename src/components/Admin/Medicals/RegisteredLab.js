import React ,{Component} from 'react'
import {  Table,Tag, Space,Col,Form,Select,Input  }from 'antd';
import {SearchOutlined } from '@ant-design/icons';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
import './Medicals.css';
//const { Column, ColumnGroup } = Table;
const { Option } = Select;

const columns = [
    {
      title: 'Lab ID',
      dataIndex: 'labId',
         // specify the condition of filtering result
      // here is that finding the labId started with `value`
      onFilter: (value, record) => record.labId.indexOf(value) === 0,
      sorter: (a, b) => a.labId.length - b.labId.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'Lab Name',
      dataIndex: 'name',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Address',
      dataIndex: 'address',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.address.length - b.address.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Contact',
        dataIndex: 'contact',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.contact.length - b.contact.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      labId: `LBOOO${i}`,
      name: `St. Tomas Hospital ${i}`,
      address: `23, Bode Thomas, Lagos state. ${i}`,
     contact: `0812345678 `,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class RegisteredLab extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>
          <h5 className="text-dark-grey heading-06" >REISTERED MEDICAL LAB</h5>
        
 <Form
      name="filter"
  className="flex medic row filter"
  gutter={[16, 16]}
    >
      
   <Col  className="col-sm-6 col-md-3">
      <Form.Item name="state" label="State " >
         <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="">Lagos</Option>
          
        </Select>
      </Form.Item>
        </Col>
     <Col  className="col-sm-6 col-md-3">
      
       <Form.Item name="lga" label="LGA " >
         <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="">Alausa</Option>
          
        </Select>
      </Form.Item>
       
        </Col>
        <Col  className="col-sm-6 col-md-3 mt-3" >
  
        <Input placeholder="Search For Labs"suffix={<SearchOutlined className="site-form-item-icon" />}  className="search"/>
        </Col>
    
     
    </Form>
      
        

<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>
<Col  className="col-sm-12 col-md-6  mb-2" style={{display:'flex'}}>
        <Button type="submit"name="submit" className=" mr-2" >ADD NEW</Button>
<Button type="submit"name="submit"  grey>DELETE MULTIPLE</Button>
</Col>

</>
        )
}
  
}


export default RegisteredLab