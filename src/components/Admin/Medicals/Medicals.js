import React ,{useState,useEffect} from 'react'
import {  Row} from "react-bootstrap";
import {  Form, Input, Space ,Col  } from 'antd';
import FileUpload from './FileUpload';
import Button from "../../../elements/Button/Button";
import {  UserOutlined, MinusCircleOutlined, PlusOutlined   } from '@ant-design/icons';
import RegisteredLab from './RegisteredLab';
import ScheduledAppointments from './ScheduledAppointments';

const Medicals = () => {
    const [form] = Form.useForm();
    const [, forceUpdate] = useState(); // To disable submit button at the beginning.
  
    useEffect(() => {
      forceUpdate({});
    }, []);
  
    const onFinish = (values) => {
      console.log('Finish:', values);
    }
  return (
<div className="pl-40 pl-10">
<div className="mt-4 mb-4">
          <h5 style={{color:'#F26527'}} className="heading-01">Medicals</h5>
<h5 className="pl-0 text-dark-grey col-12 " >Here you can add medical labs, schedule medical appointments and upload medical reports.</h5>
          
   </div> 
   <h5 className="text-dark-grey heading-06" >UPLOAD MEDICAL REPORT</h5>{/* 
  <Row className="space "><h6 className=" text-dark-grey heading-06" >UPLOAD MEDICAL REPORT</h6></Row> */} 
 
  
 <Form name="domestic worker report"onFinish={onFinish} autoComplete="off"  layout="inline"form={form} className="mt-4 mb-4" >

 <Form.Item
        name="DW ID"
        rules={[
          {
            required: true,
            message: 'Please input the DW ID!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="DW ID" />
      </Form.Item>
     <FileUpload/>
  
      <Form.List name="dw">
        {(fields, { add, remove }) => (
          <>
            {fields.map(field => (
              <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                 <Form.Item
        name="DW ID"
        rules={[
          {
            required: true,
            message: 'Please input the DW ID!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="DW ID" />
      </Form.Item>
      <FileUpload/>
                <MinusCircleOutlined onClick={() => remove(field.name)} />
              </Space>
            ))}
             <Form.Item>
            <Button type="button"   onClick={() => add()} block icon={<PlusOutlined />} grey>
              <span role="img" aria-label="plus" class="p-2">
                <svg viewBox="64 64 896 896" focusable="false" data-icon="plus" width="1em" height="1em" fill="currentColor" aria-hidden="true"><defs><style></style></defs><path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path><path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path></svg></span><span>Add More</span>
              </Button>
              </Form.Item>
              
           
          </>
        )}
      </Form.List>
      <Col  className="col-12 p-0 mt-4" style={{display:'flex'}}>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
</Col>

  
      </Form>
      <RegisteredLab/>
      <ScheduledAppointments/>
</div>
  )
}



export default Medicals