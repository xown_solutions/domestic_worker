import React from 'react';
import dateformat from "dateformat";
import {  Col, Form} from "react-bootstrap";
import Button from "../../../elements/Button/Button";
import Card from "../../../elements/Card/Card";

import './AddMessage.css';

import { DatePicker} from 'antd';

const AddMessage = () => {
    const today = new Date();

    const onChange =(date, dateString)=> {
      console.log(date, dateString);
    }
     
  return (
<div className="pl-10">
<Card Message className="message ">
<Form
              noValidate
            /*   onSubmit={this.handleSubmit}
           
              validated={this.state.validated} */
              >
          
              <Form.Row>
                  
                <Form.Group as={Col} col="12" controlId="subject">
               
                    <Form.Control
                      type="text"
                      placeholder="Message Subject"
                      name="subject"
                      id="subject"
                      /* onChange={this.handleChange} */
                      min="4"
                      required
                    />
              
                  
                </Form.Group>
                </Form.Row>
            
                <Form.Row className="selected-category">
                <Form.Label className="col-sm-6 col-md-2" >Send to:</Form.Label>
                <Form.Group as={Col} className="col-sm-6 col-md-3" controlId="dw">
             
                    <Form.Control as="select" required  >
                        <option value="">All DW</option>
                        <option value="">All DW</option>
                        </Form.Control>
                </Form.Group>
                <Form.Group as={Col}className="col-sm-6 col-md-3" controlId="engager">
              
                    <Form.Control as="select" required>
                        <option value="">All Engager</option>
                      
                        </Form.Control>
                </Form.Group>
                <Form.Group as={Col} className="col-sm-6 col-md-4 d-flex justify-content-end" controlId="date">
              
              <p>   {dateformat(today, "dd:mm:yyyy")}</p>
                </Form.Group>
                </Form.Row>
                <Form.Row className="message-box">
                <Form.Label  className="col-sm-6 col-md-2" >Message</Form.Label>
                <Form.Group controlId="Message"  className="col-10" required>
                   
                    <Form.Control as="textarea" rows={6} />
                </Form.Group>
                </Form.Row>
                <Form.Row >
                <Form.Label  className="col-sm-6 col-md-2">Schedule Delivery</Form.Label>
                <DatePicker onChange={onChange} />
                <Form.Group as={Col} col="6" >
              <Button type="submit"name="submit" className="pl-4  pb-2">SEND NOW </Button>
              </Form.Group>
              </Form.Row>
             
           
                </Form>
    </Card>
</div>
  )
}

export default AddMessage