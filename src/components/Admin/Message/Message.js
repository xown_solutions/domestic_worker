import React ,{Component} from 'react';
import {NavLink} from 'react-router-dom';
import {  Table, Space,Col  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
//const { Column, ColumnGroup } = Table;

const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
         // specify the condition of filtering result
      // here is that finding the title started with `value`
      onFilter: (value, record) => record.title.indexOf(value) === 0,
      sorter: (a, b) => a.title.length - b.title.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'Content',
      dataIndex: 'content',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.content.length - b.content.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Receiver',
      dataIndex: 'receiver',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.receiver.length - b.receiver.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Date/Time',
        dataIndex: 'time',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.time.length - b.time.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      title: `Update on product packages ${i}`,
      content: `Dear Dw, be aware that there will be a change ${i}`,
      receiver: `All DW; All Engagers ${i}`,
     time: `20th July 2020 ${i}`,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class Message extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
      <div className="pl-40 pl-10">
        <div className="mt-4 mb-4">
          <h5 style={{color:'#F26527'}} className="heading-01">Messaging</h5>
<h5 className="pl-0 text-dark-grey col-12 " >Here you can see all your shared messages!.</h5>
          
   </div> 
         
        <div className="row justify-content-end mb-4">
    
       
        <Col  className="col-sm-12 col-md-6  mb-2" style={{display:'flex'}}>
        <Button type="submit"name="submit" className=" pb-2" grey>WAITING LIST</Button>
<Button type="submit"name="submit" className=" pb-2"> <NavLink to='/admin/add_message' activeClassName='active'> CREATE NEW</NavLink></Button>
</Col>
</div>
<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>
<Col  className="col-sm-6 col-md-3">
<Button type="submit"name="submit" className="pl-4 pr-4  pb-2">DELETE MULTIPLE</Button>
</Col>
</div>
        )
}
  
}


export default Message