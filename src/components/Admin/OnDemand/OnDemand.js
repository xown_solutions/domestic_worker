import React from 'react';
import {  Row} from "react-bootstrap";
import OnDemandDw from './OnDemandDw'
import OnDemandRequest from './OnDemandRequests';
import Subscriptions from './Subscriptions';

 const OnDemand = () => {
    return (
        <div className="pl-40 pl-10">
          <div className="mt-4 mb-4">
          <h5 style={{color:'#F26527'}} className="heading-01">Medicals</h5>
<h5 className="pl-0 text-dark-grey col-12 " >Here you can view on-demand requests, domestic workers and subscriptions.</h5>
          
   </div> 
            <OnDemandRequest/>
            <Subscriptions/>
          <OnDemandDw/>  
        </div>
    )
}
export default OnDemand;