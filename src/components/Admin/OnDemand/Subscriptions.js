
import React ,{Component} from 'react'
import {  Table,Tag, Space,Col,Form,Select,Input  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
import '../Medicals/Medicals.css';
//const { Column, ColumnGroup } = Table;
const { Option } = Select;

const columns = [
    {
      title: 'Engaer ID',
      dataIndex: 'engagerId',
         // specify the condition of filtering result
      // here is that finding the engagerId started with `value`
      onFilter: (value, record) => record.engagerId.indexOf(value) === 0,
      sorter: (a, b) => a.engagerId.length - b.engagerId.length,
      sortDirections: ['descend','ascend'],
     
    },
    {
      title: 'Name',
      dataIndex: 'name',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend','ascend'],
    
    },
    
    {
      title: 'Time',
      dataIndex: 'time',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.time.length - b.time.length,
      sortDirections: ['descend','ascend'],
    },
    {
        title: 'Contact',
        dataIndex: 'contact',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.contact.length - b.contact.length,
        sortDirections: ['descend','ascend'],
    },
    {
        title: 'Status',
        dataIndex: 'status',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.status.length - b.status.length,
        sortDirections: ['descend','ascend'],
      },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      engagerId: `EG00${i}`,
     
     name: `Olulayo Olayemi ${i}`,
       time: `Flexible, 26th Jun - 30th Oct, 2020`,
        dwType: `Luxury`,
        contact: `0809384567` + i,
        status:`pending`,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class Subscriptions extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>
          <h5 className="text-dark-grey heading-06 mt-4 " >SUBSCRIPTIONS</h5>
        
 <Form
      name="filter"
  className="flex medic row filter"
  gutter={[16, 16]}
    >
      
   <Col  className="col-sm-6 col-md-3">
      <Form.Item name="Status" label="Status " >
         <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="">Luxury</Option>
          
        </Select>
      </Form.Item>
        </Col>
    
    
     
    </Form>
      
        

<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>


</>
        )
}
  
}


export default Subscriptions