
import React ,{Component} from 'react'
import {  Table,Tag, Space,Col,Form,Select,Input  }from 'antd';
import Button from "../../../elements/Button/Button";
import { FaPen,FaTrash } from "react-icons/fa";
import { IconContext } from "react-icons";
import '../Medicals/Medicals.css';
//const { Column, ColumnGroup } = Table;
const { Option } = Select;

const columns = [
  
    {
      title: 'Name',
      dataIndex: 'name',
       onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend','ascend'],
    
    },
    {
        title: 'Status',
        dataIndex: 'status',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.status.length - b.status.length,
        sortDirections: ['descend','ascend'],
      },
    
  
    {
        title: 'Address',
        dataIndex: 'address',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.address.length - b.address.length,
        sortDirections: ['descend','ascend'],
    },
    {
        title: 'Category',
        dataIndex: 'category',
         onFilter: (value, record) => record.name.indexOf(value) === 0,
        sorter: (a, b) => a.category.length - b.category.length,
        sortDirections: ['descend','ascend'],
    },
    
     {
      title: 'Action',
      dataIndex: 'action',
 
    },
  
    
  ];
  
  const data = [];
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
     name: `Olulayo Olayemi ${i}`,
     status:`pending`,
     address:2 + `${i}, Bode Thomas, Lagos state.`,
        category: `Editor`,
      action: <Space size="middle">
            <IconContext.Provider value={{ className: "icons text-orange" }}>
                  <FaPen/> 
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "icons text-orange" }}>
                <FaTrash/>
                </IconContext.Provider>
        
          </Space>
    });
  }
  class UserDatabase extends Component {
    state = {
      selectedRowKeys: [], // Check here to configure the default column
    };
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
      };
    
      render() {
        const { selectedRowKeys } = this.state;
        const rowSelection = {
          selectedRowKeys,
          onChange: this.onSelectChange,
          selections: [
            Table.SELECTION_ALL,
            Table.SELECTION_INVERT,
            {
              key: 'odd',
              text: 'Select Odd Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return false;
                  }
                  return true;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
            {
              key: 'even',
              text: 'Select Even Row',
              onSelect: changableRowKeys => {
                let newSelectedRowKeys = [];
                newSelectedRowKeys = changableRowKeys.filter((key, index) => {
                  if (index % 2 !== 0) {
                    return true;
                  }
                  return false;
                });
                this.setState({ selectedRowKeys: newSelectedRowKeys });
              },
            },
          ],
        };
    return (
        <>


        

<Table rowSelection={rowSelection} columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }}></Table>

<Col  className="col-sm-12 col-md-6  mb-2" style={{display:'flex'}}>
        <Button type="submit"name="submit" className="pt-2 pb-2" >ADD ADMIN</Button>
<Button type="submit"name="submit" className="pt-2 pb-2">SEND MESSAGE</Button>

<Button type="submit"name="submit" className="pl-4 pr-4 pt-2 pb-2" grey>MORE</Button>
</Col>

</>
        )
}
  
}


export default UserDatabase