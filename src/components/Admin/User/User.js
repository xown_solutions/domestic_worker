import React from 'react'
import LabFilter from './LabFilter'
import UserDatabase from './UserDatabse'
import "./User.css";
const User = () => {
  return (
<div className="pl-40 pl-10">
<div className="mt-4 mb-4">
<h5 style={{color:'#F26527'}} className="heading-01">User Database</h5>
<h5 className="pl-0 text-dark-grey col-12 " >Here you can view all users, send messages and add new adminstratives</h5>
</div>
<LabFilter/>
<UserDatabase/>
</div>
  )
}



export default User