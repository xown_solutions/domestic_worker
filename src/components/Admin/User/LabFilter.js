import React, { Component } from 'react';
import {  Col,Form,Select,Input,Radio,  }from 'antd';
import {SearchOutlined } from '@ant-design/icons';
/* import Input from "../../elements/InputField/Input";
import Button from "../../elements/Button/Button"; */
import Card from "../../../elements/Card/Card";
import './User.css';
const { Option } = Select;


class LabFilter extends Component {
  constructor(){
      super();
    this.state = {
       
    };
 
  }

    


   

  render() {
    return (
        <>
                  <h5 className="text-dark-grey heading-06 mt-4 mb-4" >FILTER</h5>
        <Card className="p-4 mb-4 userDb">
         
       
      <Form
      name="filter"
  className="flex row filter  mb-4 "
  gutter={[16, 16]}
    >
      
   <Col  className="col-sm-6 col-md-4">
       <h6   >Status   </h6> 
        <Select placeholder="All"  className="w-100  gutter-row" >
          <Option value="DW">DW</Option>
          <Option value="Engager">Engager</Option>
          <Option value="Admin">Admin</Option>
        </Select>
        <div class="ant-select w-100  gutter-row ">
        <Input placeholder="Search For Labs"suffix={<SearchOutlined className=" site-form-item-icon" />}  style={{maerginLeft:'1em'}} className="search"/>
        </div>
       
        </Col>
     <Col  className="col-sm-6 col-md-4">
       <h6   > DW Plan  </h6> 
        <Select placeholder="All"  mode="multiple"  className="w-100  gutter-row" >
          <Option value="ID verified">ID verified</Option>
          <Option value="MEDICALS verified">MEDICALS verified</Option>
        </Select>
        
      <Form.Item name="radio-group" label="On Demand">
        <Radio.Group>
          <Radio value="on-demand"className="filter-radio"></Radio>
          
        </Radio.Group>
      </Form.Item>
        </Col>
    <Col  className="col-sm-6 col-md-4" >
        <h6 > Location  </h6> 
        <Select placeholder="All States"  className="w-100  gutter-row" >
          <Option value="Lagos">Lagos</Option>
          
        </Select>
        <Select placeholder="LGA"  className="w-100  gutter-row" >
          <Option value="Alausa">Alausa</Option>
          
        </Select>
    </Col>
     
    </Form>
      
       </Card>
       </>
 
    )
  }
}

export default LabFilter