import React ,{useState,useEffect } from "react";
import {Redirect} from 'react-router-dom';
import {useHistory} from 'react-router';
import { connect } from "react-redux";
import { displayDW } from "../redux/actions/displayDwAction";
//import SignIn from "../components/SignIn/SignIn.js";
import Welcome from "../components/Welcome/Welcome";
import CarouselContainer from "../components/Carousel/Carousel";
import Celebrated from "../components/Celebrated/Celebrated";
import HowItWorks from "../components/HowItWorks/HowItWorks";
import Hero from "../components/Hero/Hero";
import SearchBar from "../components/SearchBar/Searchbar";
import FilteredMember from "../components/Member/FilteredMember";
import {SearchSucess } from "../redux/actions/searchAction";
import { fetchCategories } from "../redux/actions/fetchCategoriesAction";


const Home = ({ dwData, fetchDwData,fetchFilteredData}) => {
  useEffect(() => {
    fetchDwData();
    fetchCategories();
  }, [fetchDwData,fetchCategories]);
  const [searchField ,setSearchField] = useState("");
  const [selected ,setSelected] = useState("All");
  const [filter,setFilter] = useState([]);
  const history=useHistory();

  const onSearchChange = (event)=>{
    setSearchField(event.target.value) ;
    setSelected(event.target.value)
  console.log(searchField)
 
 }

 const handleSearch =(event)=>{
event.preventDefault();
 let filteredDw;
 if( selected =='All'){
    filteredDw = dwData.dws.map((dw) => {
    
      return dw
    })
  }
 filteredDw = dwData.dws.filter((dw) => {
    let name=dw.first_name + dw.last_name +dw.dw_type+dw.location
    return name.toLowerCase().includes(searchField.toLowerCase())
  })
  
   
 setFilter(filteredDw)
 setSearchField(filteredDw)
 setSelected(filteredDw)

  fetchFilteredData(filteredDw); 
 history.push('/results')
}


return (
    < >
    <span style={{display:"flex",justifyContent:'center'}}>
    <div className='col-md-8 col-xsm-12' >
    <SearchBar placeholder='Find Domestic Workers' onSearchChange={onSearchChange} handleSearch={handleSearch} />
 </div>
    </span>
   
      <Hero />
    
    {/*  {filter.length  && <Redirect to={{
        patname:'/results',
      state:{filter:filter }
      }}/>
 }
   */}

      <Welcome searchField={searchField} dwData={dwData} fetchDwData={fetchDwData}/>

      <Celebrated className="home" />

      <CarouselContainer className="home" />
     
          <HowItWorks  />
         
    </>
  );
};

const mapStateToprops = (state) => {
  return {
    dwData: state.displayDw,
    categories: state.fetchCategories,
   search: state.search,
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    fetchDwData: () => dispatch(displayDW()),
    fetchCategories: () => dispatch(fetchCategories()),
    fetchFilteredData: (search_result) => dispatch(SearchSucess(search_result))
  };
};
export default connect(mapStateToprops, mapDispatchToprops)( Home);
