import React from 'react';
import { Link } from "react-router-dom";
import { Row} from "react-bootstrap";
import Button from "../elements/Button/Button";
import NotFound from "../assets/images/404.png";

const Four04 = () => {
    return (
        <>
        <Row className="hero d-flex justify-content-center " >
        <img src={NotFound} alt="Four04" className="Four04" />
        <div class="scanlines"> </div>
        </Row>
        <Row className="hero d-flex justify-content-center " >
    
        <Button className="mr-2 m-4 " grey>
            <Link to="/" className="small">
             GO BACK TO HOME
            </Link>
          </Button> 
        </Row>
     
        </>
    )
}

export default Four04
