import React ,{useState,useEffect } from "react";
import {Redirect} from 'react-router-dom';
import {useHistory} from 'react-router';
import { connect } from "react-redux";
import { displayDW } from "../redux/actions/displayDwAction";

import {SearchSucess } from "../redux/actions/searchAction";

const Filtered = ({ dwData, search,fetchDwData,fetchFilteredData}) => {
    useEffect(() => {
        fetchDwData();
      }, [fetchDwData]);
      const [searchField ,setSearchField] = useState("");
      const [filter,setFilter] = useState([]);
      const history=useHistory();

      const onSearchChange = (event)=>{
        setSearchField(event.target.value) ;
      console.log(searchField)
     
     }
  
     const handleSearch =(event)=>{
    event.preventDefault();
    
      const filteredDw =dwData.dws.filter((dw) => {
        let name=dw.first_name + dw.last_name +dw.dw_type+dw.location
        return name.toLowerCase().includes(searchField.toLowerCase())
      })
      
      setFilter(filteredDw)
     
      fetchFilteredData(filteredDw);
      history.push('/results')
     /*  if(search.searchResult.length !== 0 ||search.searchResult.length > 0){
      history.push('/results');         
    }*/
      }   
      
    return (
        <div>
           {props.children} 
        </div>
    )
}


const mapStateToprops = (state) => {
  return {
    dwData: state.displayDw,
   search: state.search,
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    fetchDwData: () => dispatch(displayDW()),
  
    fetchFilteredData: (search_result) => dispatch(SearchSucess(search_result))
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(Filtered) 