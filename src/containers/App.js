import React from "react";
import {BrowserRouter as  Router, Route, Switch } from "react-router-dom";
import "./App.css";
import  'antd/dist/antd.css';

/*==routes==*/
import User from '../routes/ProtectedRoutes';
import Guest from '../routes/UnProtectedRoutes';
import Admin from '../routes/AdminRoutes';
/*===pages==*/
import Settings from "../components/Settings/Settings.js";
import Wallet from '../components/Wallet/Wallet';
 import SignUp from "../components/SignUp/SignUp.js";
import SignIn from "../components/SignIn/SignIn.js"; 
import About from "../components/About/About";
import Contact from "../components/Contact/Contact";
import Home from "../Pages/home";
import Four04 from "../Pages/Four04";
import Results from "../Pages/results";
import { Provider } from "react-redux";
import store from "../redux/store/store";

import Dashboard from "../components/Dashboard/Dashboard";
import Trainings from "../components/Trainings/Trainings";
import Message from "../components/Message/Message";
import Calender from "../components/Calender/Calender";
import Medicals from "../components/Medicals/Medicals";
import Faq from "../components/Faq/Faq"
import DWs from "../Pages/DWs";
/*====Admin===*/
import AddMessage from "../components/Admin/Message/AddMessage";
import MedicalRec from "../components/Admin/Medicals/Medicals";
import Billings from "../components/Admin/Billings/Bills";
import AdminSettings from "../components/Admin/Settings/Settings.js";
import MessageBoard from "../components/Admin/Message/Message.js";
import OnDemand from "../components/Admin/OnDemand/OnDemand";
import UserRecord from "../components/Admin/User/User.js";

const App = () => {
  
  return (
    <Provider store={store}>
      <Router >
       <Switch>
          
          <Guest exact path="/" component={Home} />

          <Guest path="/results" component={Results} />
          <Guest path="/about_us" component={About } />
          <Guest path="/contact_us" component={Contact} />
          <Guest path="/domestic_workers" component={DWs} />
          <Guest path="/faq" component={Faq} />
          <User path="/settings" component={Settings}/>
          <User path="/dashboard" component={Dashboard} />
          <User path="/trainings" component={Trainings} />
          <User path="/medicals" component={Medicals} />
          <User path="/wallet" component={Wallet} />
          <User path="/message" component={Message} />
          <User path="/my_calendar" component={Calender} /> 
          <Route  path="/signIn" component={SignIn} />
          <Route path="/signUp" component={SignUp} />
          {/* Admin */}
          <Admin path="/admin_add_message" component={AddMessage} />
        <Admin path="/admin_medicals" component={MedicalRec} /> 
          <Admin path="/admin_billings" component={Billings} />
          <Admin path="/admin_settings" component={AdminSettings}/>
          <Admin path="/admin_message_board" component={MessageBoard}/>
            <Admin path="/admin_on_demand" component={OnDemand}/>
            <Admin path="/admin_users_record" component={UserRecord}/>
            
          <Route  component={Four04} />
        </Switch>
      
      </Router>
    </Provider>
  );
};

export default App;
