import React,{useRef,useState} from 'react';
import Button from "../elements/Button/Button";
import { FaAngleUp } from "react-icons/fa";
import { IconContext } from "react-icons";
import ErrorBoundry from "./ErrorBoundry"
import Header from '../components/Header/Header';
import AdminSidebar from "../components/SideBar/AdminSidebar";



const AdminWrapper = (props) => {
  console.log(props)
  const myBtn =useRef();
  window.onscroll = ()=> scrollFunction();
  
  const scrollFunction=()=>{
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      myBtn.current.style.display = "block";
    } else {
      myBtn.current.style.display = "none";
    } 
  }

const goTotop=()=>{
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
const wrapper =useRef() ;
const [isOpened, setIsOpened] = useState(false);

const toggler=(event)=>{
       event.preventDefault();
       setIsOpened(!isOpened)
       console.log(isOpened)
      
  
}
return(
  <>
  <Header />
  <ErrorBoundry>
  <div id="wrapper" ref={wrapper} className={ isOpened ? "toggled" : "" } >
    
    <AdminSidebar 
        toggler={ toggler }
      
     />
        <div style={{ paddingTop: 64 ,minHeight:'100vh'}} className="pl-40">
            {props.children}
           </div>
           </div>
            <Button onClick={goTotop} ref={myBtn} id="myBtn" title="Go to top">
             <IconContext.Provider value={{ className: "icons" }}>
                  <FaAngleUp />
                </IconContext.Provider>
                
          </Button>
          </ErrorBoundry>
       
    </> 
 
)
}

export default AdminWrapper