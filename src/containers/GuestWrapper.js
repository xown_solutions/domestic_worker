import React,{useRef} from 'react';
import Button from "../elements/Button/Button";
import { FaAngleUp } from "react-icons/fa";
import { IconContext } from "react-icons";
import ErrorBoundry from "./ErrorBoundry"
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer'


const GuestWrapper = (props) => {
  console.log(props)
  const myBtn =useRef();
  window.onscroll = ()=> scrollFunction();
  
  const scrollFunction=()=>{
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      myBtn.current.style.display = "block";
    } else {
      myBtn.current.style.display = "none";
    } 
  }

const goTotop=()=>{
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
return(
  <>
  <Header />
  <ErrorBoundry>

        <div style={{paddingTop: 64 ,minHeight:'100vh',}}>
            {props.children}
           </div>
            <Button onClick={goTotop} ref={myBtn} id="myBtn" title="Go to top">
             <IconContext.Provider value={{ className: "icons" }}>
                  <FaAngleUp />
                </IconContext.Provider>
                
          </Button>
          </ErrorBoundry>
        <Footer />
    </> 
 
)
}

export default GuestWrapper