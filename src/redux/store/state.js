/* eslint-disable import/prefer-default-export */
/**
 * @constant initialState
 * @description initial state of App
 */

export const initialState = {
  userProfile: {},
  dws: [],
  driver:[],
  househelp:[],
  cleaner:[],
  categories:[],
  isAuthorized:false,
  token:'',
  categoriesCount:[],
  engagerCategories:[],
  identification:[],
  engagers: [],
  success:[],
users:[],
  searchResult:[],
  error: [],
  loading: false,
};
