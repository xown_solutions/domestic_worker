import {
    SUBMIT_ENGAGER_DATA,
    SUBMIT_ENGAGER_SUCCESS,
    SUBMIT_ENGAGER_FAIL,
  } from "../constants/constants";
  import axios from "axios";
  /* 
 
  const key = "2d0c49b28f134f4907a106fff814d7bb";
  
  /*action creator which returns action*/

  export const submitEngagerData = () => {
    return {
      type: SUBMIT_ENGAGER_DATA,
    };
  };
  export const submitEngagerSuccess = (engagers) => {
    return {
      type: SUBMIT_ENGAGER_SUCCESS,
      payload: engagers,
    };
  };
  export const submitEngagerError = (error) => {
    return {
      type: SUBMIT_ENGAGER_FAIL,
      payload: error,
    };
  };
  /*action creator  that returns a function through thunk which allows side effects like async api calls
  the app dispatch the user request to submit data which sets loading to true
  on getting response from api it dispacth success thereby submitting data
  if it doesnt get data it dispatch error   https://api.domesticworkers.com.ng/api/dw/register ,
                  }*/
  export const submitEngager = (engagers) => {
    return (dispatch) => {
      dispatch(submitEngagerData());
      delete engagers.confirmPassword;
      delete engagers.validated;
      delete engagers.error;
  
      axios({
        method: "post",
        headers: { 'Content-Type': 'application/json'},
       url:`https://api.domesticworkers.com.ng/api/engager/register `,
       data:{
        ...engagers
       } 
      
      })
        .then((response) => {
          const engagers = response.data;
          dispatch(submitEngagerSuccess(engagers),console.log(engagers));
        })
        .catch((error) => {
          const errors= error.message
          const errorMsg =error.response.data.errors
        
      if(error.response){ dispatch(submitEngagerError(errorMsg),console.log(errorMsg));
      } else if(error.request){
            dispatch(submitEngagerError(errors)); 
          } /* else{
            switch (error.status) {
              case 500:
                dispatch(submitEngagerError('We Encountered An Internal Error, Try Again...'));
                break;
              case 422:
               
                break;
           default:
                dispatch(submitEngagerError('unable to submit data,please try again'));
            break;
            } 
          } */
    
        });
    };
  };
  