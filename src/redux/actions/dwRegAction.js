import {
  SUBMIT_DW_DATA,
  SUBMIT_DW_SUCCESS,
  SUBMIT_DW_FAIL,
} from "../constants/constants";
import axios from "axios";
/* 
const intialState = {
  loading: false,
  dw: [],
  error:""
  
};
const key = "2d0c49b28f134f4907a106fff814d7bb";

/*action creator which returns action*/

export const submitDwData = () => {
  return {
    type: SUBMIT_DW_DATA,
  };
};
export const submitDwSuccess = (users) => {
  return {
    type: SUBMIT_DW_SUCCESS,
    payload: users,
  };
};
export const submitDwError = (error) => {
  return {
    type: SUBMIT_DW_FAIL,
    payload: error,
  };
};
/*action creator  that returns a function through thunk which allows side effects like async api calls
the app dispatch the user request to submit data which sets loading to true
on getting response from api it dispacth success thereby submitting data
if it doesnt get data it dispatch error   https://api.domesticworkers.com.ng/api/dw/register ,
                }*/
export const submitDW = (users) => {
  return (dispatch) => {
    dispatch(submitDwData());
    delete users.confirmPassword;
    delete users.validated;
    delete users.error;

    axios({
      method: "post",
      headers: { 'Content-Type': 'application/json'},
     url:`https://api.domesticworkers.com.ng/api/dw/register `,
     data:{
      ...users
     } 
    
    })
      .then((response) => {
        const dw = response.data;
        dispatch(submitDwSuccess(dw));
      })
      .catch((error) => {
        const errors= error.message
          const errorMsg =error.response.data.errors
        
      if(error.response){ dispatch(submitDwError(errorMsg));  console.log(errorMsg)}
     else if(error.status){
      switch (error.status) {
        case 500:
          dispatch(submitDwError('We Encountered An Internal Error, Try Again...'));
          break;
        case 422:
            dispatch(submitDwError('WeAre Unablr To Process Your Data, Try Again...'));
         
          break;
     default:
          dispatch(submitDwError('unable to submit data,please try again'));
      break;
      } 
     }
       
       else if(error.request){
            dispatch(submitDwError(errors)); 
          } 
    
        });
  };
};
