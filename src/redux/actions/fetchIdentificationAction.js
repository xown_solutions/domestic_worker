import {
    FETCH_ALL_IDENTIFICATION,
    FETCH_IDENTIFICATION_SUCCESS,
    FETCH_IDENTIFICATION_FAIL,
  } from "../constants/constants";
  import axios from "axios";
  import { API_CODE_ERROR } from "../constants/APIconfig";
 
  
  export const getIdentificationData = () => {
    return {
      type: FETCH_ALL_IDENTIFICATION,
    };
  };
  export const getIdentificationSuccess = (categories) => {
    return {
      type: FETCH_IDENTIFICATION_SUCCESS,
      payload: categories,
    };
  };
  export const getIdentificationFailed = (error) => {
    return {
      type: FETCH_IDENTIFICATION_FAIL,
      payload: error,
    };
  };
  /*action creator  that returns a function through thunk which allows side effects like async api calls
    the app dispatch the user request to submit data which sets loading to true
    on getting response from api it dispacth success thereby getting data
    if it doesnt get data it dispatch error  .slice(0,5)  ,
                    }*/
  export const fetchIdentification= () => {
  
    return (dispatch) => {
      
      dispatch(getIdentificationData());
      axios({
        method: "get",
  
        url: `https://api.domesticworkers.com.ng/api/means-of-identification`,
      })
        .then((response) => {
        
          const identification = response.data.identifications;
          
          dispatch(getIdentificationSuccess(identification));
        })
        .catch((error) => {
          
              dispatch(
                getIdentificationFailed(
                  error.message ? error.message : API_CODE_ERROR[error.status]
                )
              );
             
        });
    };
  };
  