import {
  FETCH_ALL_DW,
  FETCH_DW_SUCCESS,
  FETCH_DRIVER_SUCCESS,
  FETCH_CLEANER_SUCCESS,
  FETCH_HOUSEHELP_SUCCESS,
  FETCH_DW_FAIL,
} from "../constants/constants";
import axios from "axios";
import { API_CODE_ERROR } from "../constants/APIconfig";
/* 
  const intialState = {
    loading: false,
    dw: [],
    error:""
    
  };
  const key = "2d0c49b28f134f4907a106fff814d7bb";
  
  /*action creator which returns action*/

export const getDwData = () => {
  return {
    type: FETCH_ALL_DW,
    
  };
};
export const getDwSucess = (domestic_workers) => {
  return {
    type: FETCH_DW_SUCCESS,
    payload: domestic_workers,
  };
};
export const getDriverSucess = (drivers) => {
  return {
    type: FETCH_DRIVER_SUCCESS,
    payload: drivers,
  };
}
export const getHouseHelpSucess =(househelp) => {
  return {
    type: FETCH_HOUSEHELP_SUCCESS,
    payload: househelp,
  };
}
export const getCleanerSucess =(cleaner) => {
  return {
    type: FETCH_CLEANER_SUCCESS,
    payload: cleaner,
  };
}
export const getDwFailed = (error) => {
  return {
    type: FETCH_DW_FAIL,
    payload: error,
  };
};
/*action creator  that returns a function through thunk which allows side effects like async api calls
  the app dispatch the user request to submit data which sets loading to true
  on getting response from api it dispacth success thereby getting data
  if it doesnt get data it dispatch error  .slice(0,5)  ,
                  }*/
export const displayDW = () => {
  
  return (dispatch) => {
   
    dispatch(getDwData());
    axios({
      method: "get",

      url: `https://api.domesticworkers.com.ng/api/users`,
    })
      .then((response) => {
        const dw = response.data.slice(0,5);
    
        dispatch(getDwSucess(dw));

     
      })
      .catch((error) => {
        switch (error.status) {
          case 500:
            dispatch(
              getDwFailed("We Encountered An Internal Error, Try Again...")
            );
          
            break;
          case 404:
            dispatch(getDwFailed("No Domestic worker Has Been Registered"));
          
            break;
          case 403:
            dispatch(
              getDwFailed(
                "You're Not Authorized To View This Profile, As You're Not Logged In With Correct Token..."
              )
             
            );
          
            break;
          default:
            dispatch(
              getDwFailed(
                error.message ? error.message : API_CODE_ERROR[error.status]
              )
            );
            break;
        }
      });
  };
};
export const displayDrivers = () => {
  
  return (dispatch) => {
   
    dispatch(getDwData());
    axios({
      method: "get",

      url: `https://api.domesticworkers.com.ng/api/users`,
    })
      .then((response) => {
        const dw = response.data.filter((dw) => {
         
          return dw.dw_type.toLowerCase() ==='driver'
        })
    
        dispatch(getDriverSucess(dw.slice(0,5)));

     
      })
      .catch((error) => {
        switch (error.status) {
          case 500:
            dispatch(
              getDwFailed("We Encountered An Internal Error, Try Again...")
            );
            dispatch(getDwData());
            break;
          case 404:
            dispatch(getDwFailed("No Domestic worker Has Been Registered"));
            dispatch(getDwData());
            break;
          case 403:
            dispatch(
              getDwFailed(
                "You're Not Authorized To View This Profile, As You're Not Logged In With Correct Token..."
              )
             
            );
            dispatch(getDwData());
            break;
          default:
            dispatch(
              getDwFailed(
                error.message ? error.message : API_CODE_ERROR[error.status]
              )
            );
            break;
        }
      });
  };
};

export const displayHouseHelps= () => {
  
  return (dispatch) => {
   
    dispatch(getDwData());
    axios({
      method: "get",

      url: `https://api.domesticworkers.com.ng/api/users`,
    })
      .then((response) => {
        const dw = response.data.filter((dw) => {
         
          return dw.dw_type.toLowerCase() ==='househelp'
        })
    
        dispatch(getHouseHelpSucess(dw.slice(0,5)));

     
      })
      .catch((error) => {
        switch (error.status) {
          case 500:
            dispatch(
              getDwFailed("We Encountered An Internal Error, Try Again...")
            );
            dispatch(getDwData());
            break;
          case 404:
            dispatch(getDwFailed("No Domestic worker Has Been Registered"));
            dispatch(getDwData());
            break;
          case 403:
            dispatch(
              getDwFailed(
                "You're Not Authorized To View This Profile, As You're Not Logged In With Correct Token..."
              )
             
            );
            dispatch(getDwData());
            break;
          default:
            dispatch(
              getDwFailed(
                error.message ? error.message : API_CODE_ERROR[error.status]
              )
            );
            break;
        }
      });
  };
};

export const displayCleaners= () => {
  
  return (dispatch) => {
   
    dispatch(getDwData());
    axios({
      method: "get",

      url: `https://api.domesticworkers.com.ng/api/users`,
    })
      .then((response) => {
        const dw = response.data.filter((dw) => {
         
          return dw.dw_type.toLowerCase() ==='cleaner'
        })
    
        dispatch(getCleanerSucess(dw.slice(0,5)));

     
      })
      .catch((error) => {
        switch (error.status) {
          case 500:
            dispatch(
              getDwFailed("We Encountered An Internal Error, Try Again...")
            );
            dispatch(getDwData());
            break;
          case 404:
            dispatch(getDwFailed("No Domestic worker Has Been Registered"));
            dispatch(getDwData());
            break;
          case 403:
            dispatch(
              getDwFailed(
                "You're Not Authorized To View This Profile, As You're Not Logged In With Correct Token..."
              )
             
            );
            dispatch(getDwData());
            break;
          default:
            dispatch(
              getDwFailed(
                error.message ? error.message : API_CODE_ERROR[error.status]
              )
            );
            break;
        }
      });
  };
};
