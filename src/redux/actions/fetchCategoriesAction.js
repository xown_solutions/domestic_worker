import {
    FETCH_ALL_CATEGORIES,
    FETCH_CATEGORIES_SUCCESS,
    FETCH_CATEGORIES_FAIL,
    FETCH_ALL_CATEGORIESCOUNT,
    FETCH_CATEGORIESCOUNT_SUCCESS,
    FETCH_CATEGORIESCOUNT_FAIL,
    FETCH_ALL_ENGAGERCATEGORIES,
    FETCH_ENGAGERCATEGORIES_SUCCESS,
    FETCH_ENGAGERCATEGORIES_FAIL
  } from "../constants/constants";
  import axios from "axios";
  import { API_CODE_ERROR } from "../constants/APIconfig";
 
  
  export const getCategoriesData = () => {
    return {
      type: FETCH_ALL_CATEGORIES,
    };
  };
  export const getCategoriesSuccess = (categories) => {
    return {
      type: FETCH_CATEGORIES_SUCCESS,
      payload: categories,
    };
  };
  export const getCategoriesFailed = (error) => {
    return {
      type: FETCH_CATEGORIES_FAIL,
      payload: error,
    };
  };
  export const getCategoriesCountData = () => {
    return {
      type: FETCH_ALL_CATEGORIESCOUNT,
    };
  };
  export const getCategoriesCountSuccess = (categoriesCount) => {
    return {
      type: FETCH_CATEGORIESCOUNT_SUCCESS,
      payload: categoriesCount,
    };
  };
  export const getCategoriesCountFailed = (error) => {
    return {
      type: FETCH_CATEGORIESCOUNT_FAIL,
      payload: error,
    };
  };
  export const getEngagerCategoriesData = () => {
    return {
      type: FETCH_ALL_ENGAGERCATEGORIES,
    };
  };
  export const getEngagerCategoriesSuccess = (engagerCategories) => {
    return {
      type: FETCH_ENGAGERCATEGORIES_SUCCESS,
      payload: engagerCategories,
    };
  };
  export const getEngagerCategoriesFailed = (error) => {
    return {
      type: FETCH_ENGAGERCATEGORIES_FAIL,
      payload: error,
    };
  };
  /*action creator  that returns a function through thunk which allows side effects like async api calls
    the app dispatch the user request to submit data which sets loading to true
    on getting response from api it dispacth success thereby getting data
    if it doesnt get data it dispatch error  .slice(0,5)  ,
                    }*/
  export const fetchCategories= () => {
  
    return (dispatch) => {
      
      dispatch(getCategoriesData());
      axios({
        method: "get",
  
        url: `https://api.domesticworkers.com.ng/api/categories`,
      })
        .then((response) => {
        
          const categories = response.data.categories;
          dispatch(getCategoriesSuccess(categories));
        })
        .catch((error) => {
          
              dispatch(
                getCategoriesFailed(
                  error.message ? error.message : API_CODE_ERROR[error.status]
                )
              );
             
        });
    };
  };
  export const fetchCategoriesCount= () => {
  
    return (dispatch) => {
      
      dispatch(getCategoriesCountData);
      axios({
        method: "get",
  
        url: `https://api.domesticworkers.com.ng/api/homepage`,
      })
        .then((response) => {
        
          const categoriesCount = response.data.categories_count;
          dispatch(getCategoriesCountSuccess(categoriesCount));
        })
        .catch((error) => {
          
              dispatch(
                getCategoriesCountFailed(
                  error.message ? error.message : API_CODE_ERROR[error.status]
                )
              );
             
        });
    };
  };
  export const fetchEngagerCategories= () => {
  
    return (dispatch) => {
      
      dispatch(getEngagerCategoriesData);
      axios({
        method: "get",
  
        url: `https://api.domesticworkers.com.ng/api/engager-types`,
      })
        .then((response) => {
        
          const EngagerCategories = response.data.engager_types;
          dispatch(getEngagerCategoriesSuccess(EngagerCategories));
        })
        .catch((error) => {
          
              dispatch(
                getEngagerCategoriesFailed(
                  error.message ? error.message : API_CODE_ERROR[error.status]
                )
              );
             
        });
    };
  };
  