import {
    FETCH_ALL_IDENTIFICATION,
    FETCH_IDENTIFICATION_SUCCESS,
    FETCH_IDENTIFICATION_FAIL,
  } from "../constants/constants";
  import { initialState } from "../store/state";
  
  const fetchIdentificationReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_ALL_IDENTIFICATION:
        return {
          ...state,
          loading: true,
        };
      case FETCH_IDENTIFICATION_SUCCESS:
        return {
          ...state,
          loading: false,
        identification: action.payload,
          error:[],
        };
      case FETCH_IDENTIFICATION_FAIL:
        return {
          ...state,
          loading: false,
          identification:[],
          error: action.payload,
        };
      default:
        return state;
    }
  };
  export default fetchIdentificationReducer;
  