import { combineReducers } from "redux";
import dwRegReducer from "./dwRegReducer";
import searchReducer from './searchReducer'
import engagerRegReducer from "./engagerRegReducer";
import displayDwReducer from "./displayDwReducer";
import fetchCategoriesReducer from "./fetchCategoriesReducer";
import fetchIdentificationReducer from "./fetchIdentificationReducer";
import loginReducer from "./loginReducer";
import logoutReducer from "./logoutReducer";

const rootReducer = combineReducers({
    dwReg:dwRegReducer,
    search:searchReducer,
    logout:logoutReducer,
    login:loginReducer,
    engagerReg:engagerRegReducer ,
    displayDw:displayDwReducer,
    fetchCategories:fetchCategoriesReducer,
    fetchIdentification:fetchIdentificationReducer
});
export default rootReducer;
