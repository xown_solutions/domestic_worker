import {
  SUBMIT_DW_DATA,
  SUBMIT_DW_SUCCESS,
  SUBMIT_DW_FAIL,
} from "../constants/constants";
import { initialState } from "../store/state";

const dwRegReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUBMIT_DW_DATA:
      return {
        ...state,
        loading: true,
      };
    case SUBMIT_DW_SUCCESS:
      return {
        ...state,
        loading: false,
         success:action.payload,
        error: [],
      };
    case SUBMIT_DW_FAIL:
      return {
        ...state,
        loading: false,
        success:[],
        error: action.payload,
      };
    default:
      return state;
  }
};
export default dwRegReducer;
