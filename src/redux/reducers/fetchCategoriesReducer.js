import {
    FETCH_ALL_CATEGORIES,
    FETCH_CATEGORIES_SUCCESS,
    FETCH_CATEGORIES_FAIL,
    FETCH_ALL_CATEGORIESCOUNT,
    FETCH_CATEGORIESCOUNT_SUCCESS,
    FETCH_CATEGORIESCOUNT_FAIL,
    FETCH_ALL_ENGAGERCATEGORIES,
    FETCH_ENGAGERCATEGORIES_SUCCESS,
    FETCH_ENGAGERCATEGORIES_FAIL
  } from "../constants/constants";
  import { initialState } from "../store/state";
  
  const fetchCategoriesReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_ALL_CATEGORIES:
        return {
          ...state,
          loading: true,
        };
      case FETCH_CATEGORIES_SUCCESS:
        return {
          ...state,
          loading: false,
         categories: action.payload,
          error: [],
        };
      case FETCH_CATEGORIES_FAIL:
        return {
          ...state,
          categories:[],
          error: action.payload,
        };
        case FETCH_ALL_CATEGORIESCOUNT:
        return {
          ...state,
          loading: true,
        };
      case FETCH_CATEGORIESCOUNT_SUCCESS:
        return {
          ...state,
          loading: false,
         categoriesCount: action.payload,
          error: [],
        };
      case FETCH_CATEGORIESCOUNT_FAIL:
        return {
          ...state,
          categoriesCount:[],
          error: action.payload,
        };
        case FETCH_ALL_ENGAGERCATEGORIES:
          return {
            ...state,
            loading: true,
          };
        case FETCH_ENGAGERCATEGORIES_SUCCESS:
          return {
            ...state,
            loading: false,
           engagerCategories: action.payload,
            error: [],
          };
        case FETCH_ENGAGERCATEGORIES_FAIL:
          return {
            ...state,
          loading: false,
            engagerCategories:[],
            error: action.payload,
          };
      default:
        return state;
    }
  };
  export default fetchCategoriesReducer;
  