import {
  FETCH_ALL_DW,
  FETCH_DW_SUCCESS,
  FETCH_DRIVER_SUCCESS,
  FETCH_CLEANER_SUCCESS,
  FETCH_HOUSEHELP_SUCCESS,
  FETCH_DW_FAIL,
} from "../constants/constants";
import { initialState } from "../store/state";

const displayDwReducer = (state = initialState, action) => {
  switch (action.type) {
 


    case FETCH_ALL_DW:
      return {
        ...state,
        loading: !state.loading
      }
    case FETCH_DW_SUCCESS:
      return {
        ...state,
      loading:false,
        dws: action.payload,
        error: [],
      };
      case FETCH_DRIVER_SUCCESS:
      return {
        ...state,
      loading:false,
        driver: action.payload,
        error: [],
      };
      case FETCH_HOUSEHELP_SUCCESS:
        return {
          ...state,
        loading:false,
          househelp: action.payload,
          error: [],
        };
        case FETCH_CLEANER_SUCCESS:
          return {
            ...state,
          loading:false,
            cleaner: action.payload,
            error: [],
          };
    case FETCH_DW_FAIL:
      return {
        ...state,
        loading: false,
        dws:[],
        error: action.payload,
      }; 
      
    
    default:
      return state;
  }
};
export default displayDwReducer;
