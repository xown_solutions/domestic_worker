import {
    SUBMIT_ENGAGER_DATA,
    SUBMIT_ENGAGER_SUCCESS,
    SUBMIT_ENGAGER_FAIL,
  } from "../constants/constants";
  import { initialState } from "../store/state";
  
  const engagerRegReducer = (state = initialState, action) => {
    switch (action.type) {
      case SUBMIT_ENGAGER_DATA:
        return {
          ...state,
          loading: true,
        };
      case SUBMIT_ENGAGER_SUCCESS:
        return {
          ...state,
          loading: false,
          success: action.payload,
       
        };
      case SUBMIT_ENGAGER_FAIL:
        return {
          ...state,
          loading: false,
        success:[],
            error: action.payload,
         
        };
      default:
        return state;
    }
  };
  export default engagerRegReducer;
  