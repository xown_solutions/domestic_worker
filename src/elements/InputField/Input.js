import styled , { css } from "styled-components";

const Input = styled.div`
 
  input ,select,textarea{
    border: 1px solid #F26527 ;
  border-radius:4px;
  font-size: 12px;
  
 
  },
 input,select{
    height:52px;
  }
 textarea{
    height:187px;
  }
  &&::placeholder {
    color: #707070;
    opacity: 1;
    line-height: normal !important;
 
  &&:focus,
   {
    box-shadow: 0 0 5px #707070 !important;
    border: 2px solid #707070 !important;
  }
  ${(props) =>
    props.result &&
    css`
      background: #707070;
      color: #FFFF !important;
      
    `}; 
`;

export default Input;
