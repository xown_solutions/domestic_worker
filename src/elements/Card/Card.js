//import React from "react";
import styled , { css } from "styled-components";

const Card = styled.div`
box-shadow: 0px 3px 6px #00000029;
border-radius: 0px 10px 10px 10px;
  //box-shadow: 10px 10px 10px 0px rgba(0, 0, 0, 0.16);
 
  //border-radius:0 12px 12px 12px;
  // margin: 26px 0 26px 0;
  ${(props) =>
    props.account &&
    css`
  padding: 1rem;
    // margin: 1rem;
    color:#A7A6A6;
    // min-width: 220px;
    width:80%
    `
  };
  ${(props) =>
    props.bank &&
    css`
    box-shadow: 0px 3px 12px #F265275C;
    max-height: 150px;
    `
  };
  ${(props) =>
    props.billing &&
    css`
  
    max-height: 77px;
    width:80%;
    `
  };
  ${(props) =>
    props.pills &&
    css`
    border-radius: 30px;
    max-height: 46px;
    background: #A7A6A6;
    `
  };
  ${(props) =>
    props.Message &&
    css`
    margin:0 1rem;
    padding:1rem;
    background: #fff;
    border: 1px solid #F26527A8;
    `
  };
  ${(props) =>
    props.howItWorks &&
    css`
    max-width: 358px;
  
    height: 485px;
    
    `
  };
`;

export default Card;
