import styled, { css } from "styled-components";

const Button = styled.button`
  background: #F26527;
  border-radius: 3px;
  border: 2px solid #F26527;
  color: #ffff !important;
  // margin: 0 0.1em;
  //height: 32px;
  padding: 4px 15px;
  //font-size: 14px;
  margin-right: .5rem!important;
  margin-left: .5rem!important;
  font-weight: 400;
    white-space: nowrap;
    text-align: center;
/* && a{
  padding: 1em;
} */
  &&:hover,
  a:hover {
    background: transparent !important;
    color: #F26527 !important;
    text-decoration: none;
  }

  ${(props) =>
    props.primary &&
    css`
      background: transparent;
      color: #F26527 !important;
      &&:hover {
        border-color: #707070;
        color: #707070;
        text-decoration: none;
      }
    `};
  ${(props) =>
    props.grey &&
    css`
      background: #707070;
      border-color: #707070;
      color: #ffff;
      &&:hover,
      a:hover {
        background: transparent;
        color: #707070 !important;
        text-decoration: none;
      }
    `};
  ${(props) =>
    props.error &&
    css`
      background: red;
      color: #ffff;
      &&:hover {
        background: transparent;
        color: red;
      }
    `};
  ${(props) =>
    props.success &&
    css`
      background: green;
      color: #ffff;
      &&:hover {
        background: transparent;
        color: green;
      }
    `};
    ${(props) =>
      props.notify &&
      css`
        
background: #F26527;
box-shadow: 0px 3px 6px #00000029;
border-radius: 0px 10px 10px 10px;

        &&:hover {
          background: transparent;
          color: #fff;
        }
      `};
`;

export default Button;
