import React, { useEffect } from "react";
import { connect } from "react-redux";
import { displayDW } from "../../redux/actions/displayDwAction";
import Card from "../../elements/Card/Card";
import Button from "../../elements/Button/Button";
import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BsStarFill, BsStarHalf } from "react-icons/bs";
import { IconContext } from "react-icons";
import member from "../../assets/images/member/member3.jpg";

const Member = ({ dwData, fetchDwData }) => {
  useEffect(() => {
    fetchDwData();
  }, []);
  return dwData.loading ? (
    <h6>loading..</h6>
  ) : dwData.error ? (
    <h6>{dwData.error}</h6>
  ) : (
    <div className="d-flex flex-wrap justify-content-center">
      {dwData &&
        dwData.dws &&
        dwData.dws.map((dw) => (
         
            <Card className="member p-3 col-xsm-12 col-md-4 col-xl-3 m-2">
              <Col xs xl={12} className="d-flex flex-wrap ">
                <img
                  src={member}
                  alt="profile"
                  style={{ borderRadius: 50 }}
                  className="col-xsm-8 col-md-7"
                />

                <Col xs xl={5}>
                  <p className="small">
                   {/*  {dw.name} */} Adebayo Jhonson
                    <br />
                   {/*  {dw.company.name} */} Nanny
                  </p>
                  
                </Col>
              
              </Col>
              <Col className="d-flex justify-content-center">
                <Button className="mt-2 col-8">
                  <Link to="/profile:id" className="link small">
                    View Profile
                  </Link>
                </Button>
              </Col>
              <Col>
                <p className="small">
                  {" "}
                  <IconContext.Provider value={{ className: "search" }}>
                    <BsStarFill />
                  </IconContext.Provider>
                  <IconContext.Provider value={{ className: "search" }}>
                    <BsStarFill />
                  </IconContext.Provider>
                  <IconContext.Provider value={{ className: "search" }}>
                    <BsStarFill />
                  </IconContext.Provider>
                  <IconContext.Provider value={{ className: "search" }}>
                    <BsStarFill />
                  </IconContext.Provider>
                  <IconContext.Provider value={{ className: "search" }}>
                    <BsStarHalf />
                  </IconContext.Provider>
                  &nbsp;<b>4.5/5</b>(5 jobs)
                </p>
              </Col>

              <div className="verified ">
                <hr className="light" />
                <button disabled className=" mb-2">
                  ID VERIFIED
                </button>
                <button disabled className=" mb-2">
                  REFREE VERIFIED
                </button>
                <button disabled className=" mb-2">
                  GAURANTOR
                </button>
                <button disabled className="mb-2">
                  MEDICALS
                </button>
              </div>
            </Card>
        
        ))}
    </div>
  );
  /*  return (
    <>
     
    
      <Card className="member p-3">
        <Col xs xl={12} className="d-flex flex-wrap ">
          <img
            src={member}
            alt="profile"
            style={{ borderRadius: 50 }}
            className="col-xsm-8 col-md-7"
          />

          <Col xs xl={5}>
            <p className="small">
             {dw.name}
              <br />
              {company.bs}
            </p>
          </Col>
          <hr style={{ color: "#707070" }} />
        </Col>
        <Col className="d-flex justify-content-center">
          <Button className="mt-2 col-12">
            <Link to="/profile:id" className="link small">
              View Profile
            </Link>
          </Button>
        </Col>
        <Col>
          <p className="small">
            {" "}
            <IconContext.Provider value={{ className: "search" }}>
              <BsStarFill />
            </IconContext.Provider>
            <IconContext.Provider value={{ className: "search" }}>
              <BsStarFill />
            </IconContext.Provider>
            <IconContext.Provider value={{ className: "search" }}>
              <BsStarFill />
            </IconContext.Provider>
            <IconContext.Provider value={{ className: "search" }}>
              <BsStarFill />
            </IconContext.Provider>
            <IconContext.Provider value={{ className: "search" }}>
              <BsStarHalf />
            </IconContext.Provider>
            &nbsp;<b>4.5/5</b>(5 jobs)
          </p>
        </Col>

        <div className="verified ">
          <hr className="light" />
          <button disabled className=" mb-2">
            ID VERIFIED
          </button>
          <button disabled className=" mb-2">
            REFREE VERIFIED
          </button>
          <button disabled className=" mb-2">
            GAURANTOR
          </button>
          <button disabled className="mb-2">
            MEDICALS
          </button>
        </div>
      </Card>
    </>
  ); */
};
const mapStateToprops = (state) => {
  return {
    dwData: state.displayDw,
  };
};
const mapDispatchToprops = (dispatch) => {
  return {
    fetchDwData: () => dispatch(displayDW()),
  };
};
export default connect(mapStateToprops, mapDispatchToprops)(Member);
